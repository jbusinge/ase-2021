import csv
import ssl

import pandas as pd
import numpy as np

from collab.collaboration import dates, repos_issues, repos_prs

ssl._create_default_https_context = ssl._create_unverified_context


def main():
    path_data = '../data/'

    df_data = pd.read_csv(path_data + 'hardfork_details_final_v1.csv', sep=';')
    mainline = df_data.mainline.values.tolist()
    total_issues_ml = df_data.total_issues_ml.values.tolist()
    total_pr_ml = df_data.total_pr_ml.values.tolist()
    issue_users_ml = df_data.issue_users_ml.values.tolist()
    issue_commenters_ml = df_data.issue_commenters_ml.values.tolist()
    pr_users_ml = df_data.pr_users_ml.values.tolist()
    pr_commenters_ml = df_data.pr_commenters_ml.values.tolist()

    variant = df_data.variant.values.tolist()
    total_issues_vr = df_data.total_issues_vr.values.tolist()
    total_pr_vr = df_data.total_pr_vr.values.tolist()
    issue_users_vr = df_data.issue_users_vr.values.tolist()
    issue_commenters_vr = df_data.issue_commenters_vr.values.tolist()
    pr_users_vr = df_data.pr_users_vr.values.tolist()
    pr_commenters_vr = df_data.pr_commenters_vr.values.tolist()

    common_issue_users = df_data.common_issue_users.values.tolist()
    common_issue_commenters = df_data.common_issue_commenters.values.tolist()
    common_pr_users = df_data.common_pr_users.values.tolist()
    common_pr_commenters = df_data.common_pr_commenters.values.tolist()

    text_common_issue_users = df_data.text_common_issue_users.values.tolist()
    text_common_issue_commenters = df_data.text_common_issue_commenters.values.tolist()
    text_common_pr_users = df_data.text_common_pr_users.values.tolist()
    text_common_pr_commenters = df_data.text_common_pr_commenters.values.tolist()

    text_issue_users_ml = df_data.text_issue_users_ml.values.tolist()
    text_issue_commenters_ml = df_data.text_issue_commenters_ml.values.tolist()
    text_pr_users_ml = df_data.text_pr_users_ml.values.tolist()
    text_pr_commenters_ml = df_data.text_pr_commenters_ml.values.tolist()

    text_issue_users_vr = df_data.text_issue_users_vr.values.tolist()
    text_issue_commenters_vr = df_data.text_issue_commenters_vr.values.tolist()
    text_pr_users_vr = df_data.text_pr_users_vr.values.tolist()
    text_pr_commenters_vr = df_data.text_pr_commenters_vr.values.tolist()

    dict_keys = ['mainline', 'total_issues_ml', 'total_pr_ml', 'issue_users_ml', 'issue_commenters_ml', 'pr_users_ml',
                 'pr_commenters_ml', 'variant', 'total_issues_vr', 'total_pr_vr', 'issue_users_vr',
                 'issue_commenters_vr', 'pr_users_vr', 'pr_commenters_vr',
                 'common_issue_users', 'common_issue_commenters', 'common_pr_users', 'common_pr_commenters',
                 'text_common_issue_users', 'text_common_issue_commenters', 'text_common_pr_users',
                 'text_common_pr_commenters',
                 'text_issue_users_ml', 'text_issue_commenters_ml', 'text_pr_users_ml', 'text_pr_commenters_ml',
                 'text_issue_users_vr', 'text_issue_commenters_vr', 'text_pr_users_vr', 'text_pr_commenters_vr'
                 ]

    data_file = open(path_data + 'hardfork_details_final_v2.csv', mode='w', newline='',
                     encoding='utf-8')
    data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer.writerow(dict_keys)

    for i in range(len(mainline)):
        print(mainline[i])
        text_issue_users_ml1 = []
        text_issue_commenters_ml1 = []
        text_pr_users_ml1 = []
        text_pr_commenters_ml1 = []
        text_issue_users_vr1 = []
        text_issue_commenters_vr1 = []
        text_pr_users_vr1 = []
        text_pr_commenters_vr1 = []

        if not pd.isna(text_issue_users_ml[i]):
            text_issue_users_ml11 = text_issue_users_ml[i].split('/')
            for obj in text_issue_users_ml11:
                last_char_index = obj.rfind("-")
                text_issue_users_ml1.append(obj[:last_char_index] + "#" + obj[last_char_index + 1:])

        if not pd.isna(text_issue_commenters_ml[i]):
            text_issue_commenters_ml11 = text_issue_commenters_ml[i].split('/')
            for obj in text_issue_commenters_ml11:
                last_char_index = obj.rfind("-")
                text_issue_commenters_ml1.append(obj[:last_char_index] + "#" + obj[last_char_index + 1:])

        if not pd.isna(text_pr_users_ml[i]):
            text_pr_users_ml11 = text_pr_users_ml[i].split('/')
            for obj in text_pr_users_ml11:
                last_char_index = obj.rfind("-")
                text_pr_users_ml1.append(
                    obj[:last_char_index] + "#" + obj[last_char_index + 1:])

        if not pd.isna(text_pr_commenters_ml[i]):
            text_pr_commenters_ml11 = text_pr_commenters_ml[i].split('/')
            for obj in text_pr_commenters_ml11:
                last_char_index = obj.rfind("-")
                text_pr_commenters_ml1.append(
                    obj[:last_char_index] + "#" + obj[last_char_index + 1:])

        if not pd.isna(text_issue_users_vr[i]):
            text_issue_users_vr11 = text_issue_users_vr[i].split('/')
            for obj in text_issue_users_vr11:
                last_char_index = obj.rfind("-")
                text_issue_users_vr1.append(
                    obj[:last_char_index] + "#" + obj[last_char_index + 1:])

        if not pd.isna(text_issue_commenters_vr[i]):
            text_issue_commenters_vr11 = text_issue_commenters_vr[i].split('/')
            for obj in text_issue_commenters_vr11:
                last_char_index = obj.rfind("-")
                text_issue_commenters_vr1.append(
                    obj[:last_char_index] + "#" + obj[last_char_index + 1:])

        if not pd.isna(text_pr_users_vr[i]):
            text_pr_users_vr11 = text_pr_users_vr[i].split('/')
            for obj in text_pr_users_vr11:
                last_char_index = obj.rfind("-")
                text_pr_users_vr1.append(
                    obj[:last_char_index] + "#" + obj[last_char_index + 1:])

        # annotating the text_common_issue_users with number user touches
        text_common_issue_users1 = []
        if not pd.isna(text_common_issue_users[i]):
            text_common_issue_usrs = text_common_issue_users[i].split('/')
            text_issue_usrs_ml = text_issue_users_ml[i].split('/')
            text_issue_usrs_vr = text_issue_users_vr[i].split('/')
            for j in range(len(text_common_issue_usrs)):
                txt = ''
                for j1 in range(len(text_issue_usrs_ml)):
                    if text_common_issue_usrs[j] in text_issue_usrs_ml[j1]:
                        txt = text_issue_usrs_ml[j1]
                        break
                for j2 in range(len(text_issue_usrs_vr)):
                    if text_common_issue_usrs[j] in text_issue_usrs_vr[j2]:
                        splt = text_issue_usrs_vr[j2].split('#')
                        txt += '#' + splt[1]
                        break
                text_common_issue_users1.append(txt)

        # annotating the text_common_issue_commenters with number user touches
        text_common_issue_commenters1 = []
        if not pd.isna(text_common_issue_commenters[i]):
            text_common_issue_usrs = text_common_issue_commenters[i].split('/')
            text_issue_usrs_ml = text_issue_commenters_ml[i].split('/')
            text_issue_usrs_vr = text_issue_commenters_vr[i].split('/')
            for j in range(len(text_common_issue_usrs)):
                txt = ''
                for j1 in range(len(text_issue_usrs_ml)):
                    if text_common_issue_usrs[j] in text_issue_usrs_ml[j1]:
                        txt = text_issue_usrs_ml[j1]
                        break
                for j2 in range(len(text_issue_usrs_vr)):
                    if text_common_issue_usrs[j] in text_issue_usrs_vr[j2]:
                        splt = text_issue_usrs_vr[j2].split('#')
                        txt += '#' + splt[1]
                        break
                text_common_issue_commenters1.append(txt)

        # annotating the text_common_pr_users with number user touches
        text_common_pr_users1 = []
        if not pd.isna(text_common_pr_users[i]):
            text_common_issue_usrs = text_common_pr_users[i].split('/')
            text_issue_usrs_ml = text_pr_users_ml[i].split('/')
            text_issue_usrs_vr = text_pr_users_vr[i].split('/')
            for j in range(len(text_common_issue_usrs)):
                txt = ''
                for j1 in range(len(text_issue_usrs_ml)):
                    if text_common_issue_usrs[j] in text_issue_usrs_ml[j1]:
                        txt = text_issue_usrs_ml[j1]
                        break
                for j2 in range(len(text_issue_usrs_vr)):
                    if text_common_issue_usrs[j] in text_issue_usrs_vr[j2]:
                        splt = text_issue_usrs_vr[j2].split('#')
                        txt += '#' + splt[1]
                        break
                text_common_pr_users1.append(txt)

        # annotating the text_common_pr_commenters with number user touches
        text_common_pr_commenters1 = []
        if not pd.isna(text_common_pr_commenters[i]):
            text_common_issue_usrs = text_common_pr_commenters[i].split('/')
            text_issue_usrs_ml = text_pr_commenters_ml[i].split('/')
            text_issue_usrs_vr = text_pr_commenters_vr[i].split('/')
            for j in range(len(text_common_issue_usrs)):
                txt = ''
                for j1 in range(len(text_issue_usrs_ml)):
                    if text_common_issue_usrs[j] in text_issue_usrs_ml[j1]:
                        txt = text_issue_usrs_ml[j1]
                        break
                for j2 in range(len(text_issue_usrs_vr)):
                    if text_common_issue_usrs[j] in text_issue_usrs_vr[j2]:
                        splt = text_issue_usrs_vr[j2].split('#')
                        txt += '#' + splt[1]
                        break
                text_common_pr_commenters1.append(txt)


        data_writer_ml = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer_ml.writerow([
            mainline[i], total_issues_ml[i], total_pr_ml[i], issue_users_ml[i],
            issue_commenters_ml[i],
            pr_users_ml[i], pr_commenters_ml[i],
            variant[i], total_issues_vr[i], total_pr_vr[i], issue_users_vr[i],
            issue_commenters_vr[i], pr_users_vr[i], pr_commenters_vr[i],
            common_issue_users[i], common_issue_commenters[i], common_pr_users[i],
            common_pr_commenters[i],
            '/'.join(map(str, text_common_issue_users1)),
            '/'.join(map(str, text_common_issue_commenters1)),
            '/'.join(map(str, text_common_pr_users1)),
            '/'.join(map(str, text_common_pr_commenters1)),
            '/'.join(map(str, text_issue_users_ml1)),
            '/'.join(map(str, text_issue_commenters_ml1)),
            '/'.join(map(str, text_pr_users_ml1)),
            '/'.join(map(str, text_pr_commenters_ml1)),
            '/'.join(map(str, text_issue_users_vr1)),
            '/'.join(map(str, text_issue_commenters_vr1)),
            '/'.join(map(str, text_pr_users_vr1)),
            '/'.join(map(str, text_pr_commenters_vr1))
        ])
    data_file.close()


if __name__ == '__main__':
    main()
