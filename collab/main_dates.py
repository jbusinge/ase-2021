import csv
import ssl

import pandas as pd

from collab import collaboration

ssl._create_default_https_context = ssl._create_unverified_context


def main():
    path = '../data/'
    data_file = open(path + 'Repos_collab30_1.csv', mode='w', newline='', encoding='utf-8')
    data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer.writerow(
        ['No', 'mainline_repo', 'pr_ml', 'issues_ml', 'contrib_ml', 'created_at_ml', 'updated_at_ml',
         'archived_ml', 'variant_repo', 'pr_vr', 'issues_vr', 'contrib_vr', 'created_at_ml', 'updated_at_ml',
         'archived_ml'])

    df_data = pd.read_csv(path + 'Repos_collab30.csv', sep=';')
    No = df_data.No.values.tolist()
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()
    issues_ml = df_data.issues_ml.values.tolist()
    pr_ml = df_data.pr_ml.values.tolist()
    contrib_ml = df_data.contributors_ml.values.tolist()

    issues_vr = df_data.issues_vr.values.tolist()
    pr_vr = df_data.pr_vr.values.tolist()
    contrib_vr = df_data.contributors_vr.values.tolist()

    ct = 0
    counter = 0
    for i in range(len(mainline_repo)):
        print(i, mainline_repo[i])

        created_at_ml, updated_at_ml, archived_ml, ct = collaboration.dates(mainline_repo[i], ct)
        print('mainline = '+ created_at_ml, updated_at_ml, archived_ml)
        created_at_vr, updated_at_vr, archived_vr, ct = collaboration.dates(variant_repo[i], ct)
        print('variant = ' + created_at_vr, updated_at_vr, archived_vr)
        print()

        data_writer.writerow(
            [No[i], mainline_repo[i], pr_ml[i], issues_ml[i], contrib_ml[i], created_at_ml, updated_at_ml, archived_ml,
             variant_repo[i], pr_vr[i], issues_vr[i], contrib_vr[i], created_at_vr, updated_at_vr, archived_vr]
        )
    data_file.close()


if __name__ == '__main__':
    main()
