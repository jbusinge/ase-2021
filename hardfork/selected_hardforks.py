import csv
import ssl

import dateutil
import pandas as pd

from hardfork.collaboration import dates, commits, isses_collect

ssl._create_default_https_context = ssl._create_unverified_context


def is_not_blank(s):
    return bool(s and s.strip())


def main():
    path_data = '../data/'

    date_update = '2020-02-01T00:00:00Z'
    date_create = '2018-02-01T00:00:00Z'
    date_update1 = dateutil.parser.parse(date_update)
    date_create1 = dateutil.parser.parse(date_create)

    dict_keys = ['No', 'mainline', 'created_at_ml', 'updated_at_ml', 'variant', 'created_at_vr', 'updated_at_vr']

    df_data = pd.read_csv(path_data + 'hardfork.csv', sep=';')
    mainline = df_data.mainline.values.tolist()
    variant = df_data.variant.values.tolist()
    ct = 0
    lst = []
    a = 1
    for i in range(len(mainline)):
        created_at_vr, updated_at_vr, archived_vr, ct = dates(variant[i], ct)
        created_at_ml, updated_at_ml, archived_ml, ct = dates(mainline[i], ct)

        if is_not_blank(created_at_vr) and is_not_blank(created_at_ml):
            updated_at1_ml, ct = commits(mainline[i], ct)
            updated_at1_vr, ct = commits(variant[i], ct)
            updated_at_ml1 = dateutil.parser.parse(updated_at1_ml)
            updated_at_vr1 = dateutil.parser.parse(updated_at1_vr)

            created_at_vr1 = dateutil.parser.parse(created_at_vr)
            updated_at_vr1 = dateutil.parser.parse(updated_at1_vr)

            if updated_at_ml1 > date_update1 and updated_at_vr1 > date_update1 and created_at_vr1 < date_create1 and not archived_vr and not archived_ml:
                issues, ct = isses_collect(variant[i], ct)
                if issues:
                    print(a, i, mainline[i], variant[i])
                    print(updated_at_ml, updated_at_vr)

                    dict_values = [a, mainline[i], created_at_ml, updated_at1_ml, variant[i], created_at_vr, updated_at1_vr]
                    a += 1

                    dict_all = dict(zip(dict_keys, dict_values))
                    lst.append(dict_all)

                    data_file = open(path_data + 'hardfork_select.csv', mode='w', newline='',
                                     encoding='utf-8')
                    data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    data_writer.writerow(dict_keys)
                    for obj in lst:
                        data_writer_ml = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        data_writer_ml.writerow([obj['No'],
                                                 obj['mainline'], obj['created_at_ml'], obj['updated_at_ml'],
                                                 obj['variant'],
                                                 obj['created_at_vr'], obj['updated_at_vr']
                                                 ])
                    data_file.close()


if __name__ == '__main__':
    main()
