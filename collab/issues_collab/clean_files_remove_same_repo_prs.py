import pandas as pd
import os
from shutil import copyfile
from os import path


def main():
    # path_ml = '../../data/data/ml/'
    path_vr = '../../data/data_new/vr_cln/'
    path_ml_bf = '../../data/data_new/ml_bf_cln/'
    path_ml_af = '../../data/data_new/ml_af_cln/'

    path_vr1 = '../../data/data_new/vr/'
    path_ml_bf1 = '../../data/data_new/ml_bf/'
    path_ml_af1 = '../../data/data_new/ml_af/'
    path = '../../data/data/'
    # files_ml = os.listdir(path_ml)
    files_vr = os.listdir(path_vr1)
    files_ml_af = os.listdir(path_ml_af1)
    files_ml_bf = os.listdir(path_ml_bf1)

    df_data = pd.read_csv(path + 'pr_details_final_2.csv', sep=';')
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()

    for i in range(len(mainline_repo)):
        print(variant_repo[i])
        file_ml = mainline_repo[i].replace('/', '_')
        file_vr = variant_repo[i].replace('/', '_')
        # print('file_ml =' + file_ml)

        file_name_ml_af = ''
        file_name_ml_bf = ''
        file_name_vr = ''

        for count, filename in enumerate(files_ml_af):
            if filename == file_ml + '.csv':
                file_name_ml_af = file_ml + '.csv'
                break

        for count, filename in enumerate(files_ml_bf):
            if filename == file_ml + '.csv':
                file_name_ml_bf = file_ml + '.csv'
                break

        if file_ml + '.csv' in files_ml_af:
            for count, filename in enumerate(files_ml_af):
                if filename == file_ml + '.csv':
                    file_name_ml_af = file_ml + '.csv'
                    break
            df_data_vr1 = pd.read_csv(path_ml_af1 + file_name_ml_af, sep=';')
            # print(df_data_vr1.columns)
            # print('before', len(df_data_vr1))

            lst = []
            for j in range(len(df_data_vr1)):
                if not df_data_vr1.iloc[j]['repo_origin'] or df_data_vr1.iloc[j]['repo_origin'] == df_data_vr1.iloc[j]['repo_destin']:
                    lst.append(df_data_vr1.index[j])
            print(lst)
            df_data_vr1.drop(df_data_vr1.index[lst], inplace=True)
            # print('after', len(df_data_vr1))
            r, c = df_data_vr1.shape
            if r > 0:
                df_data_vr1.to_csv(path_ml_af + file_name_ml_af, index=False)

        if file_ml + '.csv' in files_ml_af:
            df_data_ml_af = pd.read_csv(path_ml_af1 + file_name_ml_af, sep=';', index_col='pr_no')

        if file_ml + '.csv' in files_ml_bf:
            df_data_ml_bf = pd.read_csv(path_ml_bf1 + file_name_ml_af, sep=';', index_col='pr_no')

        # copyfile(path_vr + file_name_vr, path_vr1 + file_name_vr)
        # copyfile(path_ml_bf + file_name_ml_bf, path_ml_bf1 + file_name_ml_bf)
        # copyfile(path_ml_af + file_name_ml_af, path_ml_af1 + file_name_ml_af)


if __name__ == '__main__':
    main()
