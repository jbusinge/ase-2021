# This script divides the metrics into two: 1) metrics before the fork date of the fork 1st release date
# 2) metrics after the fork date. It also collects the metrics of the of the variant after the forks first release date
import csv
import os
import pandas as pd
from collab.collaboration import dates, dates_ml
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

def main():
    path_ml = '../../data/data_new_1/ml/'
    path_ml_bf = '../../data/data_new_1/ml_bf/'
    path_ml_af = '../../data/data_new_1/ml_af/'
    path = '../../data/data_new_1/'

    df_data = pd.read_csv(path + 'pr_details_diff_22-33_rel.csv', sep=';')
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()
    release_date = df_data.release_date.values.tolist()


    ct = 0
    for i in range(len(mainline_repo)):
        print(i, mainline_repo[i])

        # created_at, ct = dates_ml(variant_repo[i], ct)

        file_ml = mainline_repo[i].replace('/', '_')
        print(path_ml + file_ml + '.csv')
        df_data_ml = pd.read_csv(path_ml + file_ml + '.csv', delimiter=';')
        # print(df_data_ml.head())

        mainline_repo1 = df_data_ml.mainline_repo.values.tolist()
        pr_no = df_data_ml.pr_no.values.tolist()
        created_at1 = df_data_ml.created_at.values.tolist()
        closed_at1 = df_data_ml.closed_at.values.tolist()
        merged_at1 = df_data_ml.merged_at.values.tolist()
        repo_origin = df_data_ml.repo_origin_ml.values.tolist()
        repo_destin = df_data_ml.repo_destin.values.tolist()
        pr_owner = df_data_ml.pr_owner.values.tolist()

        no_commenters = df_data_ml.no_commenters.values.tolist()
        pr_commenters = df_data_ml.pr_commenters.values.tolist()
        pr_comments = df_data_ml.pr_comments.values.tolist()
        counter1 = 0
        counter2 = 0
        data_file_ml_bf = open(path_ml_bf + file_ml + '.csv', mode='w', newline='', encoding='utf-8')
        data_file_ml_af = open(path_ml_af + file_ml + '.csv', mode='w', newline='', encoding='utf-8')
        for j in range(len(merged_at1)):
            if merged_at1[j] < release_date[i]:
                # file_ml = mainline_repo1[i].replace('/', '_')
                if counter1 == 0:

                    data_writer_ml_bf = csv.writer(data_file_ml_bf, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    data_writer_ml_bf.writerow(
                        ['No', 'mainline_repo', 'pr_no', 'created_at', 'closed_at', 'merged_at', 'repo_origin', 'repo_destin',
                         'pr_owner', 'no_commenters', 'pr_commenters', 'pr_comments'])
                    counter1 = 1
                data_writer_ml_bf.writerow(
                    [j, mainline_repo1[j], pr_no[j], created_at1[j], closed_at1[j], merged_at1[j], repo_origin[j],
                     repo_destin[j], pr_owner[j], no_commenters[j], pr_commenters[j], pr_comments[j]]
                )
            else:
                if counter2 == 0:

                    data_writer_ml_af = csv.writer(data_file_ml_af, delimiter=';', quotechar='"',
                                                   quoting=csv.QUOTE_MINIMAL)
                    data_writer_ml_af.writerow(
                        ['No', 'mainline_repo', 'pr_no', 'created_at', 'closed_at', 'merged_at', 'repo_origin', 'repo_destin',
                         'pr_owner', 'no_commenters', 'pr_commenters', 'pr_comments'])
                    counter2 = 1
                data_writer_ml_af.writerow(
                    [j, mainline_repo1[j], pr_no[j], created_at1[j], closed_at1[j], merged_at1[j], repo_origin[j],
                     repo_destin[j], pr_owner[j], no_commenters[j], pr_commenters[j], pr_comments[j]]
                )
        data_file_ml_bf.close()
        print(path_ml_bf + file_ml + '.csv written')
        data_file_ml_af.close()
        print(path_ml_af + file_ml + '.csv written')


if __name__ == '__main__':
    main()
