import csv
import ssl

import pandas as pd

from hardfork.collaboration import fork_parent

ssl._create_default_https_context = ssl._create_unverified_context

def main():
    path_data = '../data/'
    dict_keys = ['No', 'mainline', 'created_at_ml', 'updated_at_ml', 'variant', 'created_at_vr', 'updated_at_vr', 'parent']

    df_data = pd.read_csv(path_data + 'hardfork_select.csv', sep=';')
    mainline = df_data.mainline.values.tolist()
    variant = df_data.variant.values.tolist()

    created_at_ml = df_data.created_at_ml.values.tolist()
    updated_at_ml = df_data.updated_at_ml.values.tolist()
    created_at_vr = df_data.created_at_vr.values.tolist()
    updated_at_vr = df_data.updated_at_vr.values.tolist()
    ct = 0
    lst = []
    a = 1

    for i in range(len(variant)):
        prnt = 'NO'
        parent, ct = fork_parent(variant[i], ct)
        if parent == mainline[i]:
            prnt = 'YES'
        else:
            print(a, i, mainline[i], variant[i])
            # print(updated_at_ml[i], updated_at_vr[i])


        dict_values = [a, mainline[i], created_at_ml[i], updated_at_ml[i], variant[i], created_at_vr[i], updated_at_vr[i], prnt]
        a += 1

        dict_all = dict(zip(dict_keys, dict_values))
        lst.append(dict_all)

        data_file = open(path_data + 'hardfork_select_1.csv', mode='w', newline='',
                         encoding='utf-8')
        data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(dict_keys)

        for obj in lst:
            data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer.writerow([obj['No'],
                                     obj['mainline'], obj['created_at_ml'], obj['updated_at_ml'],
                                     obj['variant'],
                                     obj['created_at_vr'], obj['updated_at_vr'], obj['parent']
                                     ])
        data_file.close()


if __name__ == '__main__':
    main()