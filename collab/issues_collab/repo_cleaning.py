
import ssl

import csv
import pandas as pd

from collab.collaboration import fork_parent

ssl._create_default_https_context = ssl._create_unverified_context


def main():
    path = '../../data/data/'

    df_data = pd.read_csv(path + 'pr_details_final_2.csv', sep=';')

    dict_keys = ['No', 'mainline_repo', 'closed_pr_ml', 'merged_pr_ml', 'variant_repo', 'closed_pr_vr', 'merged_pr_vr']

    data_file = open(path + 'pr_details_final_2_2.csv', mode='w', newline='', encoding='utf-8')
    data_writer_ml = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer_ml.writerow(
        ['No', 'mainline_repo', 'closed_pr_ml', 'merged_pr_ml', 'variant_repo', 'closed_pr_vr', 'merged_pr_vr'])
    ct = 0

    # print(df_data.columns)

    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()
    close_pr_ml = df_data.closed_pr_ml.values.tolist()
    merged_pr_ml = df_data.merged_pr_ml.values.tolist()
    close_pr_vr = df_data.closed_pr_vr.values.tolist()
    merged_pr_vr = df_data.merged_pr_vr.values.tolist()
    No = df_data.No.values.tolist()

    z = 1

    for i in range(len(variant_repo)):
        parent, ct = fork_parent(variant_repo[i], ct)
        if parent == mainline_repo[i]:
            print(No[i], z, mainline_repo[i], variant_repo[i])
            data_writer_ml = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer_ml.writerow([
                No[i], mainline_repo[i], close_pr_ml[i], merged_pr_ml[i], variant_repo[i],
                close_pr_vr[i], merged_pr_vr[i]])
            z += 1
    data_file.close()


if __name__ == '__main__':
    main()
