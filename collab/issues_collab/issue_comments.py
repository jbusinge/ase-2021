from collab import header_jsonData
from collab.collaboration import dates
import datetime
import dateutil.parser
from textblob import TextBlob
from langdetect import detect


def repos_issues_comments_ml_after(repo, created_at, stopdate, ct):
    p = 1
    total_issues = 0
    repo_comments = []
    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/issues?since=' + created_at + '&state=all&page=' + str(
            p) + '&per_page=100'
        contrib_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if contrib_arrays != None:
            if len(contrib_arrays) == 0:
                break
            for obj in contrib_arrays:
                date = obj['created_at']
                if date < stopdate:
                    number = obj['number']
                    url_comments = 'https://api.github.com/repos/' + repo + '/issues/' + str(number) + '/comments'
                    comments_array, ct = header_jsonData.getResponse(url_comments, ct)
                    # print('Issue_number = ' + str(number))
                    if comments_array != None:
                        for obj_comments in comments_array:
                            if obj_comments['body'] != None:
                                if 'bot :package::rocket' not in obj_comments['body']:
                                    b = TextBlob(obj_comments['body'])
                                    # lang = detect(obj_comments['body'])
                                    if len(obj_comments['body']) > 2 and 'en' in b.detect_language():
                                        # print(obj_comments['body'])
                                        repo_comments.append(obj_comments['body'])
                                        # print(obj_comments['body'])

            total_issues += len(contrib_arrays)
            if total_issues % 500 == 0:
                print(' ---- issues: ', total_issues)
        else:
            break
    return repo_comments, ct


def comments(issue_url, closed_at, ct):
    p = 1
    pr_comments = []
    pr_commentors = []

    url_comments = issue_url + '/comments'
    comments_array, ct = header_jsonData.getResponse(url_comments, ct)
    if comments_array is not None:
        for obj_comments in comments_array:
            created_at = obj_comments['created_at']
            if obj_comments['body'] is not None:
                if created_at < closed_at:
                    if 'bot :package::rocket' not in obj_comments['body']:
                        # b = TextBlob(obj_comments['body'])
                        # if len(obj_comments['body']) > 2 and 'en' in b.detect_language():
                        pr_commentors.append(obj_comments['user']['login'])
                        pr_comments.append(obj_comments['body'])
    return pr_comments, pr_commentors, ct
