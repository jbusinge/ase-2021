No;mainline_repo;pr_no;created_at;closed_at;merged_at;repo_origin;repo_destin;pr_owner;no_commenters;pr_commenters;pr_comments
2;gaearon/react-document-title;4;2014-10-30T11:57:21Z;2014-10-30T12:39:24Z;2014-10-30T12:39:24Z;;gaearon/react-document-title;pluma;1;gaearon;"Good catch.
"
3;gaearon/react-document-title;3;2014-10-29T14:42:09Z;2014-10-29T14:47:11Z;2014-10-29T14:47:11Z;;gaearon/react-document-title;pluma;2;gaearon/pluma;"We're taking a devDependency here on React >= 0.12 right? Should probably clarify this in `package.json`.
 You're right, I forgot to bump the package.json.
"
4;gaearon/react-document-title;2;2014-10-23T11:53:38Z;2014-10-23T15:57:38Z;2014-10-23T15:57:38Z;;gaearon/react-document-title;pluma;14;gaearon/pluma/gaearon/pluma/gaearon/pluma/gaearon/pluma/gaearon/pluma/gaearon/gaearon/pluma/gaearon;"This is really nice, thank you! I will review and merge later in the evening.
 Just FYI, this will still lead to `DocumentTitle.mountedInstances` accumulating if you use `React.renderComponentToString`.

Because you may want to get the document title on the server, I don't see any good way around this.

In my render function I just make sure to also return `DocumentTitle.mountedInstances.splice(0)[0].props.title` (as a side-effect `splice` truncates the array). This only works if the rendered component has a `DocumentTitle` at some point of the hierarchy and exposing implementation details like that is a bit iffy, but I think fixing that would be out of scope for this PR.
 > Because you may want to get the document title on the server, I don't see any good way around this.

Ah I see. I intended this component only to be used on client side because it wouldn't (in my understanding) work on server anyway. I assumed that by the time we generate `<head>` and `<title>` on server, we don't know the title. Is this wrong?

If it _is_ possible to use it on server after all, let's think what a better server-side API could look like? It would've been cool to make this a hybrid solution.
 If you render the HTML progressively (as you would in traditional CGI scripts or PHP), yes, the `<title>` would already be sent. But in most cases using node you would probably use a template library of some sort and render the React component in advance.

In my case I'm using [trumpet](https://github.com/substack/trumpet) instead of a template library so the entire thing currently looks like this:

``` js
// in my React app's ""server.jsx"":

module.exports = {
  init: (data, path) => /* do magic with data */,
  render: () => React.renderComponentToString(<App/>),
  getTitle: () => DocumentTitle.mountedInstances.splice(0)[0].props.title
};

// in my express middleware:

app.use(function (req, resp, next) {
  resp.render = function (html, title, data) {
    var tr = trumpet();
    tr.pipe(res);
    tr.createWriteStream('title').end(title);
    tr.createWriteStream('#app').end(html);
    tr.createWriteStream('#initial-state')
    .end('window.__INITIAL_STATE__ = ' + JSON.stringify(data || {}));
    fs.createReadStream(filepath).pipe(tr);
  };
  next();
});

// in my express router:

router.get('*', function (req, resp) {
  // ... get data from somewhere ...
  myReactApp.init(data, req.url);
  resp.render(myReactApp.render(), myReactApp.getTitle(), data);
});
```
 I see!

I'm mostly interested in getting this to work with react-router's server rendering. Indeed, [that seems possible](https://github.com/rackt/react-router-mega-demo/blob/master/app/server.js#L35).

So what about `DocumentTitle.getTitle()` for server usage?
 You'd still need to truncate `DocumentTitle.mountedInstances`, so I think if you do that as a side-effect, `getTitle` would be misleading. Maybe either have two methods (`getTitle` and `reset`?) or name it something else?

EDIT: TBH, I think the Flux way would be having a store instead of statics. But that's a whole 'nother can of worms (Reflux stores? Fluxxor stores? `react-flux` stores? let's not go there...).
 Ah, I realize now. Is there absolutely no other way? You don't get `componentWillUnmount` on server, but is there any other suggested way of cleaning up resources when rendering with React on server?
 None that I'm aware of.
 Okay. Any suggestions for naming? I'd go with one method. Does something like `popDocumentTitle` make sense? And we need to think of a way to warn when it isn't called in server environment.
 Generally `pop` implies only the trailing item will be removed from a collection. Maybe just something generic like `DocumentTitle.rewind`? I don't think we need to name it `DocumentTitle.somethingDocumentTitle` because that is a bit redundant (it's a method, so we will probably never refer to it on its own).

Mind if we take this conversation to [gitter](https://gitter.im/gaearon)?
 I like `rewind`. Let's do it! Would you amend the PR to include it?
 BTW we can spot something is rotten if there are many `mountedInstance`s that have `isMounted() = false`.
 There you go.

PS: I just noticed I made a mistake in my usage example earlier (new instances are appended to `mountedInstances`, not prepended, so `splice(0)[0]` will not return the current title but the first title). Good thing we're solving that with `rewind`.
 Would you mind a couple of style fixes:

``` javascript
  // <----  add a newline here
if (activeInstance) {
  return activeInstance.props.title;
}
```

and

``` javascript
      if (typeof document === 'undefined') {
        return;
      }
```

so it's consistent with the rest of the code
"
