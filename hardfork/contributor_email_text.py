import csv

import pandas as pd



def main():
    path_data = '../data/fork_of/'
    path_new = '../data/fork_of/developer_emails/'

    df_data = pd.read_csv(path_data + 'mainline_variant_developer_details_fork_of_12.csv', sep=';')
    mainline = df_data.mainline.values.tolist()
    variant = df_data.variant.values.tolist()
    mainline_maintainer_details = df_data.mainline_maintainer_details.values.tolist()
    mainline_maintainer_details = df_data.mainline_maintainer_details.values.tolist()

    for i in range(len(mainline)):
        print(i + 1, mainline[i], variant[i])
        fm = mainline[i].replace('/', '__')
        fv = variant[i].replace('/', '__')
        filename = str(i+1) + '_' + fm + '__' + fv

        fhandle = open(path_new + filename + ".txt", "w+")

        dear = 'Dear Active GitHub Contributor,\n\n'
        para1 = 'We are carrying out an empirical investigation on software families on GitHub (i.e., mainlines and ' \
                'their variants). A variant is a software repository that is forked from another repository ' \
                '(mainline) and thereafter they evolve into independent projects being maintained in parallel. ' \
                'The reason we are contacting you is because we have identified your GitHub repository \"' \
                + variant[i] + '\" as a variant of the mainline \"' + mainline[i] + '\".\n\n'
        para2 = 'I am conducting this research as a postdoctoral researcher at the University of Antwerp in the ' \
                'context of a Belgian interuniversity research collaboration. With this survey we aim to identify and' \
                ' understand the reasons that led you to create or to participate in a fork of an existing project. ' \
                'Based on the results of this survey, we plan to develop open-source recommendation tools for ' \
                'automating the extraction and integration of patches between variants in a software family.'
        para3 = 'Details of the research project and the data policy can be found in the Informed consent form that ' \
                'is part of the survey link below.\n\n'
        para4 = 'If you are actively involved in creating the aforementioned variant, kindly answer the questions ' \
                'in the survey link below. Your participation in this project consists of filling out an online ' \
                'questionnaire, lasting about 15 minutes.\n\n'
        para5 = 'To help us link your survey answers to the aforementioned variant repository, in the survey we ' \
                'shall kindly ask you to restate the name of the variant in the survey.\n\n'
        para6 = 'https://github.com/johnxu21/variants-survey/blob/main/Survey.pdf\n\n\n'
        para7 = 'Thank you so much for participating in our survey.\n\n'
        para8 = 'Best regards,\n\nJohn Businge\nPost Doctoral Research\nUniversity of Antwerp'

        fhandle.write(dear + para1 + para2 + para3 + para4 + para5 + para6 + para7 + para8)
        fhandle.close()


if __name__ == '__main__':
    main()