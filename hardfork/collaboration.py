import json

import tokens_ci
from collab import header_jsonData

import dateutil.parser


import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import requests


def repos_travis_ci(repo, ct):
    total_contrib = 0
    travis_ci_arrays = []
    headers = {'Authorization': 'Token ' + tokens_ci.allTokens[ct]}
    url_com = 'https://api.travis-ci.com/repos/' + repo + '/builds'
    url_org = 'https://api.travis-ci.org/repos/' + repo + '/builds'

    req_com = requests.get(url_com, headers=headers, verify=False)
    req_org = requests.get(url_org)
    jsonData_com = json.loads(req_com.text)
    jsonData_org = json.loads(req_org.text)

    if len(jsonData_com) != 0:
        print(len(jsonData_com))
        print(jsonData_com[0]['number'])
        print('jsonData_com success')
    elif len(jsonData_org) != 0:
        print(json.dumps(jsonData_org, indent=4))
        print(len(jsonData_org))
        print(jsonData_org[0]['number'])
        print('jsonData_org success')
    else:
        print('No Builds')

    return travis_ci_arrays, ct


def repos_contrib_counts(repo, ct):
    p = 1
    total_contrib = 0
    contrib_arrays1 = []
    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/contributors?page=' + str(p) + '&per_page=100'
        contrib_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if contrib_arrays != None:
            if len(contrib_arrays) == 0:
                break
            total_contrib += len(contrib_arrays)
            for obj in contrib_arrays:
                contrib_arrays1.append(obj['login'])
        else:
            break
    return total_contrib, contrib_arrays1, ct


def repos_merged_pr_counts(repo, stop_date, ct):
    p = 1
    total_prs = 0
    merged_pr = 0
    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/pulls?state=closed&page=' + str(p) + '&per_page=100&'
        pr_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if pr_arrays != None:
            if len(pr_arrays) == 0:
                break
            for obj in pr_arrays:
                if obj['merged_at'] is not None:
                    merged_at = obj['merged_at']
                    if merged_at < stop_date:
                        merged_pr += 1

            total_prs += len(pr_arrays)
            if total_prs % 100 == 0:
                print(' ---- pr: ', total_prs)
        else:
            break
    return total_prs, merged_pr, ct


def repos_pr_counts(repo, ct):
    p = 1
    total_prs = 0
    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/pulls?state=all&page=' + str(p) + '&per_page=100&'
        pr_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if pr_arrays != None:
            if len(pr_arrays) == 0:
                break
            total_prs += len(pr_arrays)
            if total_prs % 500 == 0:
                print(' ---- pr: ', total_prs)
        else:
            break
    return total_prs, ct


def repos_prs(repo, since, until, ct):
    p = 1
    total_issues = 0
    total_pulls = 0
    pull_users = []
    pull_commenters = []

    dict_pull_users = dict()
    dict_pull_commenters = dict()

    until1 = dateutil.parser.parse(until)
    since1 = dateutil.parser.parse(since)
    flag = 0
    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/pulls?state=all&page=' + str(
            p) + '&per_page=100&since=' + since
        issue_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if issue_arrays != None:
            if len(issue_arrays) == 0:
                break
            for obj in issue_arrays:
                created_at1 = dateutil.parser.parse(obj['created_at'])
                if created_at1 <= since1:
                    flag = 1
                    break

                if until1 >= created_at1:
                    total_pulls += 1
                    if obj['user']['login'] is not None:
                        usr = obj['user']['login']
                        pull_users.append(usr)
                        dict_pull_users[usr] = dict_pull_users.get(usr, 0) + 1

                    if obj['issue_url'] is not None:
                        comments_url = obj['issue_url'] + '/comments'
                        comments_arrays, ct = header_jsonData.getResponse(comments_url, ct)
                        if len(comments_arrays) != 0:
                            for obj1 in comments_arrays:
                                if obj1['user']['login'] is not None:
                                    usr = obj1['user']['login']
                                    pull_commenters.append(usr)
                                    dict_pull_commenters[usr] = dict_pull_commenters.get(usr, 0) + 1

            if flag == 1:
                break
            total_issues += len(issue_arrays)
            if total_issues % 500 == 0:
                print(' ---- pulls: ', total_issues)
        else:
            break

    pull_users = list(set(pull_users))
    pull_commenters = list(set(pull_commenters))

    dict_pull_users = sorted(dict_pull_users.items(), key=lambda kv: kv[1])
    dict_pull_commenters = sorted(dict_pull_commenters.items(), key=lambda kv: kv[1])

    return total_pulls, pull_users, pull_commenters, dict_pull_users, dict_pull_commenters, ct


def repos_issues(repo, since, until, ct):
    p = 1
    total_issues = 0
    total_pulls = 0
    issue_users = []
    issue_commenters = []

    dict_pull_users = dict()
    dict_pull_commenters = dict()

    until1 = dateutil.parser.parse(until)
    since1 = dateutil.parser.parse(since)
    flag = 0
    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/issues?state=all&page=' + str(
            p) + '&per_page=100&since=' + since
        issue_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if issue_arrays != None:
            if len(issue_arrays) == 0:
                break
            for obj in issue_arrays:
                created_at1 = dateutil.parser.parse(obj['created_at'])
                if created_at1 < since1:
                    flag = 1
                    break
                if until1 >= created_at1:
                    total_pulls += 1
                    if obj['user']['login'] is not None:
                        usr = obj['user']['login']
                        issue_users.append(usr)
                        dict_pull_users[usr] = dict_pull_users.get(usr, 0) + 1
                        # print(obj['user']['login'])

                    if obj['comments_url'] is not None:
                        comments_url = obj['comments_url']
                        comments_arrays, ct = header_jsonData.getResponse(comments_url, ct)
                        if len(comments_arrays) != 0:
                            for obj1 in comments_arrays:
                                if obj1['user']['login'] is not None:
                                    usr = obj1['user']['login']
                                    issue_commenters.append(usr)
                                    dict_pull_commenters[usr] = dict_pull_commenters.get(usr, 0) + 1

            if flag == 1:
                break
            total_issues += len(issue_arrays)
            if total_issues % 500 == 0:
                print(' ---- issues: ', total_issues)
        else:
            break

    issue_users = list(set(issue_users))
    issue_commenters = list(set(issue_commenters))

    dict_pull_users = sorted(dict_pull_users.items(), key=lambda kv: kv[1])
    dict_pull_commenters = sorted(dict_pull_commenters.items(), key=lambda kv: kv[1])
    return total_pulls, issue_users, issue_commenters, dict_pull_users, dict_pull_commenters, ct


def repos_issues_counts(repo, ct):
    p = 1
    total_issues = 0
    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/issues?state=all&since=' + +'&page=' + str(
            p) + '&per_page=100'
        contrib_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if contrib_arrays != None:
            if len(contrib_arrays) == 0:
                break
            total_issues += len(contrib_arrays)
            if total_issues % 500 == 0:
                print(' ---- issues: ', total_issues)
        else:
            break
    return total_issues, ct


def repos_commits_counts(repo, ct, created_at=None):
    p = 1
    total_commits = 0
    ret = False
    while True:
        if created_at is not None:
            url2 = 'https://api.github.com/repos/' + repo + '/commits?page=' + str(
                p) + '&per_page=100&since=' + created_at
        else:
            url2 = 'https://api.github.com/repos/' + repo + '/commits?page=' + str(p) + '&per_page=100'
        com_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if com_arrays is None:
            break
        if com_arrays is not None:
            if len(com_arrays) == 0:
                break
            total_commits += len(com_arrays)
        if total_commits >= 50:
            ret = True
            break
        else:
            ret = False
            break
    return total_commits, ct


def check_travis(repo, ct):
    travis = False
    url2 = 'https://api.github.com/repos/' + repo + '/contents'
    content_arrays, ct = header_jsonData.getResponse(url2, ct)
    if content_arrays is not None:
        for obj in content_arrays:
            if obj['name'] == '.travis.yml':
                travis = True
                break
    return travis, ct


def ci_integration(repo, ct):
    ci = []
    url2 = 'https://api.github.com/repos/' + repo + '/contents'
    content_arrays, ct = header_jsonData.getResponse(url2, ct)
    if content_arrays is not None:
        for obj in content_arrays:
            if obj['name'].endswith('.yml') or ('circleci' in obj['name']) or ('appveyor' in obj['name']) or (
                    'cloudbees' in obj['name']) or ('werker' in obj['name']) or ('codeship' in obj['name']) or (
                    'codegresh' in obj['name']) or ('jenkins' in obj['name']) or ('semaphore' in obj['name']):
                ci.append(obj['name'])
    return ci, ct


def commits(repo, ct):
    updated_at = '2000-02-01T00:00:00Z'
    url2 = 'https://api.github.com/repos/' + repo + '/commits'
    content_arrays, ct = header_jsonData.getResponse(url2, ct)
    if len(content_arrays) != 0:
        if content_arrays[0]['commit'] is not None:
            if content_arrays[0]['commit']['committer'] is not None:
                updated_at = content_arrays[0]['commit']['committer']['date']
    # print(updated_at)

    return updated_at, ct


def dates(repo, ct):
    created_at = ''
    updated_at = ''
    archived = ''
    url2 = 'https://api.github.com/repos/' + repo
    content_arrays, ct = header_jsonData.getResponse(url2, ct)
    if content_arrays is not None:
        created_at = content_arrays['created_at']
        updated_at = content_arrays['updated_at']
        archived = content_arrays['archived']
    return created_at, updated_at, archived, ct


def dates_ml(repo, ct):
    created_at = ''
    updated_at = ''
    archived = ''
    url2 = 'https://api.github.com/repos/' + repo
    content_arrays, ct = header_jsonData.getResponse(url2, ct)
    if content_arrays is not None:
        created_at = content_arrays['created_at']
    return created_at, ct


def fork_parent(repo, ct):
    parent = ''
    url = 'https://api.github.com/repos/' + repo
    content_arrays, ct = header_jsonData.getResponse(url, ct)
    fork = content_arrays['fork']
    if fork:
        if content_arrays['parent']['full_name'] is not None:
            parent = content_arrays['parent']['full_name']

    return parent, ct


def isses_collect(repo, ct):
    issues = False
    url = 'https://api.github.com/repos/' + repo + '/issues?state=all&per_page=100'
    content_arrays, ct = header_jsonData.getResponse(url, ct)
    if len(content_arrays) >= 10:
        # print('issues = ', len(content_arrays))
        issues = True

    return issues, ct


def fork_parent(repo, ct):
    parent = ''
    url = 'https://api.github.com/repos/' + repo
    content_arrays, ct = header_jsonData.getResponse(url, ct)
    fork = content_arrays['fork']
    if fork:
        if content_arrays['parent']['full_name'] is not None:
            parent = content_arrays['parent']['full_name']

    return parent, ct

def dev_email_mainline(repo, since, ct):
    until = add_month(since)
    dict_pull_name_details = dict()
    lst_issue_users_ml = []
    string = ''
    tot_com = 0
    p = 1
    count = 0
    while True:
        url = 'https://api.github.com/repos/' + repo + '/pulls?state=closed&sort=created&per_per=100&page=' + str(p)
        p += 1
        count += 1
        pulls_arrays, ct = header_jsonData.getResponse(url, ct)
        # tot_com += len(pulls_arrays)
        if pulls_arrays is not None:
            if len(pulls_arrays) == 0:
                break
            for obj in pulls_arrays:
                pull_created_at = obj['created_at']
                # print(pull_created_at, since)
                if dateutil.parser.parse(pull_created_at) < dateutil.parser.parse(since):
                    count = 0
                    continue

                if obj['merged_at'] is not None:
                    pull_url = obj['url']
                    pull_number_json, ct = header_jsonData.getResponse(pull_url, ct)
                    if pull_number_json['merged_by'] is not None:
                        login = pull_number_json['merged_by']['login']
                        email_url = 'https://api.github.com/users/' + login + '/events/public'
                        email_json_array, ct = header_jsonData.getResponse(email_url, ct)
                        for email_obj_array in email_json_array:
                            if email_obj_array['type'] == 'PushEvent':
                                commit_array = email_obj_array['payload']['commits']
                                for email_obj in commit_array:
                                    if (email_obj['author']['email'] is not None) and \
                                            email_obj['author']['name'] is not None and \
                                            'noreply' not in email_obj['author']['email'] and \
                                            login in email_obj_array['actor']['login']:
                                        email = email_obj['author']['email']
                                        name = email_obj['author']['name']
                                        name_details = name + "&" + email + "&" + login
                                        # print(name_details)
                                        dict_pull_name_details[name_details] = \
                                            dict_pull_name_details.get(name_details, 0) + 1
                                        break
                                    else:
                                        continue
                                    break
                                else:
                                    continue
                                break
                            else:
                                continue
                            break

        if count > 3:
            break

    dict_pull_name_details = sorted(dict_pull_name_details.items(), key=lambda kv: kv[1])
    i = 0
    for k, v in dict_pull_name_details[::-1]:
        if i < 5:
            lst_issue_users_ml.append(k + '#' + str(v))
        else:
            break
        i += 1
    string = '/'.join(map(str, lst_issue_users_ml))

    return string, ct

def dev_email_variant(repo, ct):
    dict_pull_name_details = dict()
    lst_issue_users_ml = []
    string = ''
    tot_com = 0
    p = 1
    while True:
        url = 'https://api.github.com/repos/' + repo + '/pulls?state=closed&sort=created&per_page=100&page=' + str(p)
        p += 1
        pulls_arrays, ct = header_jsonData.getResponse(url, ct)
        # tot_com += len(pulls_arrays)
        if pulls_arrays is not None:
            if len(pulls_arrays) == 0:
                break
            for obj in pulls_arrays:
                if obj['merged_at'] is not None:
                    pull_url = obj['url']
                    # print(pull_url)
                    pull_number_json, ct = header_jsonData.getResponse(pull_url, ct)

                    if pull_number_json['merged_by'] is not None:
                        login = pull_number_json['merged_by']['login']
                        email_url = 'https://api.github.com/users/' + login + '/events/public'
                        email_json_array, ct = header_jsonData.getResponse(email_url, ct)
                        for email_obj_array in email_json_array:
                            if email_obj_array['type'] == 'PushEvent':
                                commit_array = email_obj_array['payload']['commits']
                                for email_obj in commit_array:
                                    if (email_obj['author']['email'] is not None) and \
                                            email_obj['author']['name'] is not None and \
                                            'noreply' not in email_obj['author']['email'] and \
                                            login in email_obj_array['actor']['login']:
                                        email = email_obj['author']['email']
                                        name = email_obj['author']['name']
                                        name_details = name + "&" + email + "&" + login
                                        dict_pull_name_details[name_details] = \
                                            dict_pull_name_details.get(name_details, 0) + 1
                                        break
                                    else:
                                        continue
                                    break
                                else:
                                    continue
                                break
                            else:
                                continue
                            break

        if p > 3:
            break

    dict_pull_name_details = sorted(dict_pull_name_details.items(), key=lambda kv: kv[1])
    i = 0
    for k, v in dict_pull_name_details[::-1]:
        if i < 5:
            lst_issue_users_ml.append(k + '#' + str(v))
        else:
            break
        i += 1
    string = '/'.join(map(str, lst_issue_users_ml))

    return string, ct


def add_month(date):
    date1 = date.split('-')
    imonth = int(date1[1]) + 3
    iyear = int(date1[0])
    if imonth > 12:
        imonth = imonth - 12
        iyear += 1
    smonth = str(imonth)
    syear = str(iyear)
    until = syear + '-' + smonth + '-' + date1[2]

    return until
