import csv
import os
import ssl

import pandas as pd

from collab import collaboration

import requests

from collab.ci_collab.functions import builds_travis_ci, builds_circle_ci


def main():
    path = '../../data-ci/'
    path_ml = '../../data/data/ml/'
    path_vr = '../../data/data/vr/'

    files_ml = os.listdir(path_ml)
    files_vr = os.listdir(path_vr)

    df_data = pd.read_csv(path + 'pr_details_circle_travis_ci_builds.csv', sep=',')
    No = df_data.No.values.tolist()
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()
    closed_pr_ml = df_data.closed_pr_ml.values.tolist()
    merged_pr_ml = df_data.merged_pr_ml.values.tolist()
    closed_pr_vr = df_data.closed_pr_vr.values.tolist()
    merged_pr_vr = df_data.merged_pr_vr.values.tolist()
    ml_travis_builds = df_data.ml_travis_builds.values.tolist()
    ml_circle_builds = df_data.ml_circle_builds.values.tolist()
    vr_travis_builds = df_data.vr_travis_builds.values.tolist()
    vr_circle_builds = df_data.vr_circle_builds.values.tolist()
    ml_travis_pr_builds = df_data.ml_travis_pr_builds.values.tolist()
    vr_travis_pr_builds = df_data.vr_travis_pr_builds.values.tolist()
    ml_build_server = df_data.ml_build_server.values.tolist()
    vr_build_server = df_data.vr_build_server.values.tolist()

    dict_keys = ['No', 'mainline_repo', 'closed_pr_ml', 'merged_pr_ml', 'ml_all_pr_with_comments',
                 'ml_fork_pr_with_comments', 'ml_all_pr_comments', 'ml_fork_pr_comments',
                 'ml_all_pr_comment_contributors', 'ml_fork_pr_comment_contributors', 'ml_travis_builds',
                 'ml_travis_pr_builds', 'ml_circle_builds', 'variant_repo', 'closed_pr_vr', 'merged_pr_vr',
                 'vr_all_pr_with_comments', 'vr_fork_pr_with_comments', 'vr_all_pr_comments', 'vr_fork_pr_comments',
                 'vr_all_pr_comment_contributors', 'vr_fork_pr_comment_contributors', 'vr_travis_builds',
                 'vr_travis_pr_builds', 'vr_circle_builds', 'ml_build_server', 'vr_build_server']

    lst = []
    ct = 0
    for i in range(len(mainline_repo)):
        ml_all_pr_comments = 0
        ml_fork_comments = 0
        ml_all_pr_with_comments = 0
        ml_fork_pr_with_comments = 0
        ml_all_pr_comment_contributors = 0
        ml_fork_pr_comment_contributors = 0
        vr_all_pr_comments = 0
        vr_fork_comments = 0
        vr_all_pr_with_comments = 0
        vr_fork_pr_with_comments = 0
        vr_all_pr_comment_contributors = 0
        vr_fork_pr_comment_contributors = 0
        lst_ml1 = []
        lst_ml2 = []
        lst_vr1 = []
        lst_vr2 = []

        file_ml = mainline_repo[i].replace('/', '_')
        file_vr = variant_repo[i].replace('/', '_')

        print(i, 'mainline_repo = ', mainline_repo[i])

        file_name_ml = ''
        file_name_vr = ''

        for count, filename in enumerate(files_ml):
            if filename == file_ml + '.csv':
                file_name_ml = file_ml + '.csv'
                break
        if os.stat(path_ml + file_name_ml).st_size != 0:
            print(file_name_ml)
            df_data_ml = pd.read_csv(path_ml + file_name_ml, sep=';')
            row_ml, col_ml = df_data_ml.shape
            if row_ml > 0:
                for index, row in df_data_ml.iterrows():
                    ml_all_pr_comments += row['no_commenters']
                    ml_all_pr_with_comments += 1
                    lst_all_contrib = row['pr_commenters'].split('/')
                    lst_ml1 += lst_all_contrib
                    # print('no_commenters = ', row['no_commenters'])
                    if row['repo_origin_ml'] != row['repo_destin']:
                        ml_fork_comments += row['no_commenters']
                        ml_fork_pr_with_comments += 1
                        lst_fork_contrib = row['pr_commenters'].split('/')
                        lst_ml2 += lst_fork_contrib

        print('ml_all_pr_comments = ', ml_all_pr_comments)
        print('ml_fork_comments = ', ml_fork_comments)

        print(i, 'mainline_repo = ', mainline_repo[i])
        for count, filename in enumerate(files_vr):
            if filename == file_vr + '.csv':
                file_name_vr = file_vr + '.csv'
                break
        if os.stat(path_vr + file_name_vr).st_size != 0:
            print(file_name_vr)
            df_data_vr = pd.read_csv(path_vr + file_name_vr, sep=';')
            row_vr, col_vr = df_data_vr.shape
            if row_vr > 0:
                for index, row in df_data_vr.iterrows():
                    vr_all_pr_comments += row['no_commenters']
                    vr_all_pr_with_comments += 1
                    lst_all_contrib = row['pr_commenters'].split('/')
                    lst_vr1 += lst_all_contrib
                    if row['repo_origin_ml'] != row['repo_destin']:
                        vr_fork_comments += row['no_commenters']
                        vr_fork_pr_with_comments += 1
                        lst_fork_contrib = row['pr_commenters'].split('/')
                        lst_vr2 += lst_fork_contrib

        print('vr_all_pr_comments = ', vr_all_pr_comments)
        print('vr_fork_comments = ', vr_fork_comments)

        data_file = open(path + 'pr_details_comments_circle_travis_ci_builds_2.csv', mode='w', newline='',
                         encoding='utf-8')
        data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(dict_keys)

        ml_fork_pr_comment_contributors = len(list(set(lst_ml2)))
        ml_all_pr_comment_contributors = len(list(set(lst_ml1)))
        vr_fork_pr_comment_contributors = len(list(set(lst_vr2)))
        vr_all_pr_comment_contributors = len(list(set(lst_vr1)))

        dict_values = [No[i],
                       mainline_repo[i], closed_pr_ml[i], merged_pr_ml[i], ml_all_pr_with_comments,
                       ml_fork_pr_with_comments, ml_all_pr_comments, ml_fork_comments, ml_all_pr_comment_contributors,
                       ml_fork_pr_comment_contributors, ml_travis_builds[i], ml_travis_pr_builds[i], ml_circle_builds[i],
                       variant_repo[i], closed_pr_vr[i], merged_pr_vr[i], vr_all_pr_with_comments,
                       vr_fork_pr_with_comments, vr_all_pr_comments, vr_fork_comments, vr_all_pr_comment_contributors,
                       vr_fork_pr_comment_contributors, vr_travis_builds[i], vr_travis_pr_builds[i],
                       vr_circle_builds[i], ml_build_server[i], vr_build_server[i]]

        dict_all = dict(zip(dict_keys, dict_values))
        lst.append(dict_all)

        for obj in lst:
            data_writer_ml = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer_ml.writerow([
                obj['No'],
                obj['mainline_repo'], obj['closed_pr_ml'], obj['merged_pr_ml'], obj['ml_all_pr_with_comments'],
                obj['ml_fork_pr_with_comments'], obj['ml_all_pr_comments'], obj['ml_fork_pr_comments'],
                obj['ml_all_pr_comment_contributors'], obj['ml_fork_pr_comment_contributors'],
                obj['ml_travis_builds'], obj['ml_travis_pr_builds'], obj['ml_circle_builds'],
                obj['variant_repo'], obj['closed_pr_vr'], obj['merged_pr_vr'], obj['vr_all_pr_with_comments'],
                obj['vr_fork_pr_with_comments'], obj['vr_all_pr_comments'], obj['vr_fork_pr_comments'],
                obj['vr_all_pr_comment_contributors'], obj['vr_fork_pr_comment_contributors'],
                obj['vr_travis_builds'], obj['vr_travis_pr_builds'], obj['vr_circle_builds'], obj['ml_build_server'],
                obj['vr_build_server']
            ])
        data_file.close()
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')


if __name__ == '__main__':
    main()
