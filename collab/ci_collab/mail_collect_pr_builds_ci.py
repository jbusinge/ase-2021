import csv
import ssl

import pandas as pd

from collab import collaboration

import requests

from collab.ci_collab.functions import builds_travis_ci_pr


def main():
    path = '../../data-ci/'

    df_data = pd.read_csv(path + 'travis_ci_builds.csv', sep=';')
    No = df_data.No.values.tolist()
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()
    closed_pr_ml = df_data.closed_pr_ml.values.tolist()
    merged_pr_ml = df_data.merged_pr_ml.values.tolist()
    closed_pr_vr = df_data.closed_pr_vr.values.tolist()
    merged_pr_vr = df_data.merged_pr_vr.values.tolist()
    ml_total_builds = df_data.ml_total_builds.values.tolist()
    vr_total_builds = df_data.vr_total_builds.values.tolist()
    ml_build_server = df_data.ml_build_server.values.tolist()
    vr_build_server = df_data.vr_build_server.values.tolist()

    dict_keys = ['No', 'mainline_repo', 'closed_pr_ml', 'merged_pr_ml', 'variant_repo', 'closed_pr_vr', 'merged_pr_vr',
                 'ml_total_builds', 'ml_pr_builds', 'vr_total_builds', 'vr_pr_builds', 'ml_build_server',
                 'vr_build_server']
    builds_vr = []
    builds_ml = []
    lst = []
    ct = 0

    for i in range(len(mainline_repo)):
        ml_pr_builds = 0
        vr_pr_builds = 0

        print(i, 'mainline_repo = ', mainline_repo[i])
        if ml_total_builds[i] != 0:
            ml_pr_builds, ct = builds_travis_ci_pr(mainline_repo[i], ct)
        print('ml_pr_builds = ', ml_pr_builds)
        print(i, 'variant_repo = ', variant_repo[i])
        if vr_total_builds[i] != 0:
            vr_pr_builds, ct = builds_travis_ci_pr(variant_repo[i], ct)
        print('vr_pr_builds = ', vr_pr_builds)
        print('******************************')

        data_file = open(path + 'travis_ci_pr_builds.csv', mode='w', newline='', encoding='utf-8')
        data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(
            ['No', 'mainline_repo', 'closed_pr_ml', 'merged_pr_ml', 'variant_repo', 'closed_pr_vr', 'merged_pr_vr',
             'ml_total_builds', 'ml_pr_builds', 'vr_total_builds', 'vr_pr_builds', 'vr_build_server',
             'vr_build_server'])

        dict_values = [No[i], mainline_repo[i], closed_pr_ml[i], merged_pr_ml[i], variant_repo[i], closed_pr_vr[i],
                       merged_pr_vr[i], ml_total_builds[i], ml_pr_builds, vr_total_builds[i], vr_pr_builds,
                       ml_build_server[i], vr_build_server[i]]

        dict_all = dict()
        dict_all = dict(zip(dict_keys, dict_values))
        lst.append(dict_all)

        for obj in lst:
            data_writer_ml = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer_ml.writerow([
                obj['No'], obj['mainline_repo'], obj['closed_pr_ml'], obj['merged_pr_ml'], obj['variant_repo'],
                obj['closed_pr_vr'], obj['merged_pr_vr'], obj['ml_total_builds'], obj['ml_pr_builds'],
                obj['vr_total_builds'], obj['vr_pr_builds'], obj['ml_build_server'], obj['vr_build_server']
            ])
        data_file.close()


if __name__ == '__main__':
    main()
