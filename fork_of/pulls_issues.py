import csv
import ssl

import pandas as pd

from collab.collaboration import dates, repos_issues, repos_prs
from fork_of.functions import pulls, issues

ssl._create_default_https_context = ssl._create_unverified_context


def main():
    path_data = '../data/'

    df_data = pd.read_csv(path_data + 'fork_of.csv', sep=';')
    mainline = df_data.mainline.values.tolist()
    variant = df_data.variant.values.tolist()

    until = '2021-02-01T00:00:00Z'




    dict_keys = ['mainline', 'total_issues_ml', 'total_pr_ml', 'variant', 'total_issues_vr', 'total_pr_vr'
                 ]

    data_file = open('fork_of_pr_issues.csv', mode='w', newline='',
                     encoding='utf-8')
    data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer.writerow(dict_keys)

    ct = 0
    total_pulls_ml = []

    lst1 = []
    for i in range(len(mainline)):



        created_at, updated_at, archived, ct = dates(variant[i], ct)
        print(i, mainline[i])

        total_pulls_ml, ct = pulls(mainline[i], created_at, until, ct)

        total_issues_ml, ct = issues(mainline[i], created_at, until, ct)


        print(i, variant[i])

        total_pulls_vr, ct = pulls(variant[i], created_at, until, ct)

        total_issues_vr, ct = issues(variant[i], created_at, until, ct)





        dict_values = [mainline[i], total_issues_ml, total_pulls_ml,
                       variant[i], total_issues_vr, total_pulls_vr,
                       ]
        dict_all = dict(zip(dict_keys, dict_values))
        lst1.append(dict_all)



        print('***********************************')

        data_file = open('fork_of_details.csv', mode='w', newline='',
                         encoding='utf-8')
        data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(dict_keys)

        for obj in lst1:
            data_writer_ml = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer_ml.writerow([
                obj['mainline'], obj['total_issues_ml'], obj['total_pr_ml'],
                obj['variant'], obj['total_issues_vr'], obj['total_pr_vr']
            ])
        data_file.close()


if __name__ == '__main__':
    main()
