import json
import ssl
import urllib

import requests
# from urllib import request
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import requests

import tokens_ci
from collab import collaboration


# ssl._create_default_https_context = ssl._create_unverified_context


def main():
    jsonData = None
    ct = 1
    builds = 0
    pr_builds = 0
    repo = 'libero/texture'

    headers = {'Authorization': 'Token ' + tokens_ci.allTokens[ct]}
    url_com = 'https://api.travis-ci.com/repos/'+repo+'/builds'
    url_org = 'https://api.travis-ci.org/repos/' + repo + '/builds'

    req_com = requests.get(url_com, headers=headers, verify=False)
    req_org = requests.get(url_org)
    jsonData_com = json.loads(req_com.text)
    jsonData_org = json.loads(req_org.text)

    if len(jsonData_com) != 0:
        print(json.dumps(jsonData_com, indent=4))
        print(len(jsonData_com))
        print(jsonData_com[0]['number'])
        print('jsonData_com success')
    elif len(jsonData_org) != 0:
        print(json.dumps(jsonData_org, indent=4))
        print(len(jsonData_org))
        print(jsonData_org[0]['number'])
        print('jsonData_org success')
    else:
        print('No Builds')

    # after_number = jsonData[len(jsonData) - 1]['number']


    # builds += len(jsonData)

    # url_com += '&after_number=' + str(after_number)
    # url_org += '&after_number=' + str(after_number)
    #
    # while True:
    #     req = requests.get(url_com, headers=headers, verify=False)
    #     req = requests.get(url_com)
    #     jsonData = json.loads(req.text)
    #     if len(jsonData) == 0:
    #         break
    #     after_number = jsonData[len(jsonData) - 1]['number']
    #     builds += len(jsonData)
    #     print('after_number = ', after_number)
    #     print('builds = ', builds)
    #     print(json.dumps(jsonData, indent=4))
    #     # break
    #
    #
    # print(builds)
    # print(after_number)

    # ct = 0
    # jsonData, ct = collaboration.repos_travis_ci('HealthCatalyst/Fabric.Cashmere', ct)
    #
    # print(jsonData)


if __name__ == '__main__':
    main()
