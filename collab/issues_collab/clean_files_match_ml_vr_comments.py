# this file removes files with no comments from files in the folders ml_af, ml_bf, and vr

import pandas as pd
import os
from shutil import copyfile

def main():
    path_ml = '../../data/data/ml/'
    path_vr = '../../data/data/vr/'
    path_ml_bf = '../../data/data/ml_bf/'
    path_ml_af = '../../data/data/ml_af/'

    path_vr1 = '../../data/data_new/vr/'
    path_ml_bf1 = '../../data/data_new/ml_bf/'
    path_ml_af1 = '../../data/data_new/ml_af/'
    path = '../../data/data/'
    files_ml = os.listdir(path_ml)
    files_vr = os.listdir(path_vr)
    files_ml_af = os.listdir(path_ml_af)
    files_ml_bf = os.listdir(path_ml_bf)

    df_data = pd.read_csv(path + 'pr_details_final_2.csv', sep=';')
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()

    for i in range(len(mainline_repo)):
        file_ml = mainline_repo[i].replace('/', '_')
        file_vr = variant_repo[i].replace('/', '_')
        # print('file_ml =' + file_ml)
        fl_vr = variant_repo[i].replace('/', '_')

        file_name_ml_af = ''
        file_name_ml_bf = ''
        file_name_vr = ''

        for count, filename in enumerate(files_ml_af):
            if filename == file_ml + '.csv':
                file_name_ml_af = file_ml + '.csv'
                break

        for count, filename in enumerate(files_ml_bf):
            if filename == file_ml + '.csv':
                file_name_ml_bf = file_ml + '.csv'
                break
        for count, filename in enumerate(files_vr):
            if filename == file_vr + '.csv':
                file_name_vr = file_vr + '.csv'
                break

        if (os.stat(path_vr + file_name_vr).st_size != 0) and (os.stat(path_ml_bf + file_name_ml_bf).st_size != 0) and (
                os.stat(path_ml_af + file_name_ml_af).st_size != 0):
            df_data_vr = pd.read_csv(path_vr + file_name_vr, sep=';')
            df_data_ml_bf = pd.read_csv(path_ml_bf + file_name_ml_bf, sep=';')
            df_data_ml_af = pd.read_csv(path_ml_af + file_name_ml_af, sep=';')
            row_vr, col_vr = df_data_vr.shape
            row_ml_af, col_ml_af = df_data_ml_af.shape
            row_ml_bf, col_ml_bf = df_data_ml_bf.shape
            if row_vr != 0 and row_ml_af != 0 and row_ml_bf != 0:
                print(i, mainline_repo[i], variant_repo[i])
                copyfile(path_vr + file_name_vr, path_vr1 + file_name_vr)
                copyfile(path_ml_bf + file_name_ml_bf, path_ml_bf1 + file_name_ml_bf)
                copyfile(path_ml_af + file_name_ml_af, path_ml_af1 + file_name_ml_af)



if __name__ == '__main__':
    main()
