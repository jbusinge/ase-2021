import csv
import ssl

import pandas as pd

from collab import collaboration

import requests

from collab.ci_collab.functions import builds_travis_ci, builds_circle_ci


def main():
    path = '../../data-ci/'

    df_data = pd.read_csv(path + 'pr_details_ci_builds_pr.csv', sep=';')
    No = df_data.No.values.tolist()
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()
    closed_pr_ml = df_data.closed_pr_ml.values.tolist()
    merged_pr_ml = df_data.merged_pr_ml.values.tolist()
    closed_pr_vr = df_data.closed_pr_vr.values.tolist()
    merged_pr_vr = df_data.merged_pr_vr.values.tolist()
    ml_total_builds = df_data.ml_total_builds_ci.values.tolist()
    vr_total_builds = df_data.vr_total_builds_ci.values.tolist()
    ml_pr_build_ci = df_data.ml_pr_build_ci.values.tolist()
    vr_pr_builds_ci = df_data.vr_pr_builds_ci.values.tolist()
    ml_build_server = df_data.ml_build_server.values.tolist()
    vr_build_server = df_data.vr_build_server.values.tolist()

    dict_keys = ['No', 'mainline_repo', 'closed_pr_ml', 'merged_pr_ml', 'ml_travis_builds', 'ml_travis_pr_builds',
                 'ml_circle_builds', 'variant_repo', 'closed_pr_vr', 'merged_pr_vr', 'vr_travis_builds',
                 'vr_travis_pr_builds', 'vr_circle_builds', 'ml_build_server',
                 'vr_build_server']
    builds_vr = []
    builds_ml = []
    lst = []
    ct = 0
    for i in range(len(mainline_repo)):

        print(i, 'mainline_repo = ', mainline_repo[i])
        print('ml_build = ', ml_build_server[i])
        ml_circle_builds, ct = builds_circle_ci(mainline_repo[i], ct)
        print(i, 'variant_repo = ', variant_repo[i])
        print('vr_build = ', vr_build_server[i])
        vr_circle_builds, ct = builds_circle_ci(variant_repo[i], ct)
        print('******************************')

        data_file = open(path + 'pr_details_circle_travis_ci_builds.csv', mode='w', newline='', encoding='utf-8')
        data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(dict_keys)

        dict_values = [No[i], mainline_repo[i], closed_pr_ml[i], merged_pr_ml[i], ml_total_builds[i], ml_pr_build_ci[i],
                       ml_circle_builds, variant_repo[i], closed_pr_vr[i], merged_pr_vr[i], vr_total_builds[i],
                       vr_pr_builds_ci[i], vr_circle_builds, ml_build_server[i], vr_build_server[i]]

        dict_all = dict(zip(dict_keys, dict_values))
        lst.append(dict_all)

        for obj in lst:
            data_writer_ml = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer_ml.writerow([
                obj['No'], obj['mainline_repo'], obj['closed_pr_ml'], obj['merged_pr_ml'], obj['ml_travis_builds'],
                obj['ml_travis_pr_builds'], obj['ml_circle_builds'], obj['variant_repo'], obj['closed_pr_vr'],
                obj['merged_pr_vr'], obj['vr_travis_builds'], obj['vr_travis_pr_builds'], obj['vr_circle_builds'],
                obj['ml_build_server'], obj['vr_build_server']
            ])
        data_file.close()


if __name__ == '__main__':
    main()
