import csv
import accessToken
import tokens
import requests

with open('data/families.csv', newline='') as myFile:
    reader = csv.reader(myFile, delimiter=';')
    next(reader, None)
    line_count = 0
    ct = 0
    for row in reader:
        urlMlv = 'https://api.github.com/repos/' + row[2] + '/commits'
        urlFv = 'https://api.github.com/repos/' + row[5]
        remove_urlMlv = requests.get(urlMlv)
        remove_urlFv = requests.get(urlMlv)
        if (remove_urlMlv.status_code == 404) or (remove_urlFv.status_code == 404):
            print(line_count + ' - '+urlMlv + ' Deleted *******')
            continue
        commits = 0
        p = 1
        i = 0
        while True:
            if ct >= len(tokens.allTokens):
                ct = 0
            token = tokens.allTokens[ct]
            print(str(ct) + " - " +token)
            try:
                print(urlMlv + '?page=' + str(p) + '&per_page=100')
                jsonCommits = accessToken.jsonAPIReturn(urlMlv + '?page=' + str(p) + '&per_page=100', token)
                # print(jsonCommits)
                i = 0
                if len(jsonCommits) == 0:
                    print('end of repo')
                    break
                if p == 10:
                    print(jsonCommits)
                commits += len(jsonCommits)
                print(urlMlv + ' = '+len(jsonCommits))
            except:
                print('invalid ')

            line_count += 1
            ct += 1
            p += 1