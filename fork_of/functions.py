import dateutil

from collab import header_jsonData


def pulls(repo, since, until, ct):
    p = 1
    total_pulls = 0
    pull_users = []
    pull_commenters = []

    dict_pull_users = dict()
    dict_pull_commenters = dict()

    until1 = dateutil.parser.parse(until)
    since1 = dateutil.parser.parse(since)
    flag = 0
    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/pulls?state=all&page=' + str(p) + '&per_page=100'
        issue_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if issue_arrays != None:
            if len(issue_arrays) == 0:
                break
            obj = issue_arrays[len(issue_arrays) -1]
            created_at1 = dateutil.parser.parse(obj['updated_at'])
            if since1 >= created_at1:
                print('obj[created_at] =', obj['updated_at'])
                print('since = ', since)
                break
            else:
                total_pulls += len(issue_arrays)

    print('total_issues', total_pulls)


    return total_pulls, ct

def issues(repo, since, until, ct):
    p = 1
    total_issues = 0
    pull_users = []
    pull_commenters = []

    dict_pull_users = dict()
    dict_pull_commenters = dict()

    until1 = dateutil.parser.parse(until)
    since1 = dateutil.parser.parse(since)
    flag = 0
    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/issues?state=all&page=' + str(p) + '&per_page=100'
        issue_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if issue_arrays != None:
            if len(issue_arrays) == 0:
                break
            obj = issue_arrays[len(issue_arrays) -1]
            created_at1 = dateutil.parser.parse(obj['updated_at'])
            if since1 >= created_at1:
                print('obj[created_at] =', obj['updated_at'])
                print('since = ', since)
                break
            else:
                total_issues += len(issue_arrays)

    print('total_issues', total_issues)


    return total_issues, ct