import urllib.request
import json
import tokens
import tokens_ci
import requests


def getResponse_ci(url, ct):
    jsonData = None
    url = 'https://api.travis-ci.com/repos/HealthCatalyst/Fabric.Cashmere'
    ct = 1
    builds = 0

    headers = {'Authorization': 'Token ' + tokens_ci.allTokens[ct]}
    url = 'https://api.travis-ci.com/repos/HealthCatalyst/Fabric.Cashmere/builds'
    req = requests.get(url, headers=headers, verify=False)
    jsonData = json.loads(req.text)
    after_number = jsonData[len(jsonData) - 1]['number']
    # builds += len(jsonData)

    while True:
        url = 'https://api.travis-ci.com/repos/HealthCatalyst/Fabric.Cashmere/builds?after_number=' + str(after_number)
        req = requests.get(url, headers=headers, verify=False)
        jsonData = json.loads(req.text)
        if len(jsonData) == 0:
            break
        after_number = jsonData[len(jsonData) - 1]['number']
        builds += len(jsonData)
        print('after_number = ', after_number)
        print('builds = ', builds)
        print(json.dumps(jsonData, indent=4))
        break

    print(builds)
    print(after_number)

def getResponse(url,ct):
    jsonData = None
    try:
        if ct == len(tokens.allTokens):
            ct = 0
        reqr = urllib.request.Request(url)
        reqr.add_header('Authorization', 'token %s' % tokens.allTokens[ct])
        opener = urllib.request.build_opener(urllib.request.HTTPHandler(debuglevel=1))
        content = opener.open(reqr).read()
        ct += 1
        jsonData = json.loads(content)
        # return jsonData, self.ct
    except Exception as e:
        pass
        print(e)
    return jsonData, ct

def url_header(url,ct):
    jsonData = None
    try:
        if ct == len(tokens.allTokens):
            ct = 0
        reqr = urllib.request.Request(url)
        reqr.add_header('Accept', 'application/vnd.github.mercy-preview+json')
        reqr.add_header('Authorization', 'token %s' % tokens.allTokens[ct])
        opener = urllib.request.build_opener(urllib.request.HTTPHandler(debuglevel=1))
        content = opener.open(reqr).read()
        ct += 1
        jsonData = json.loads(content)
        return jsonData, ct
    except Exception as e:
        pass
        #print(e)
    return jsonData, ct


def url_header_ci(url, ct):
    # headers = {'Authorization' : 'Bearer' +access_token, 'Content - Type': 'application / json; charset = utf - 8’}
    jsonData = None
    try:
        if ct == len(tokens.allTokens):
            ct = 0
        reqr = urllib.request.Request(url)
        reqr.add_header('Accept', 'application/Travis-API-Version: 3')
        reqr.add_header('Authorization', 'token %s' % tokens_ci.allTokens[ct])
        opener = urllib.request.build_opener(urllib.request.HTTPHandler(debuglevel=1))
        content = opener.open(reqr).read()
        ct += 1
        jsonData = json.loads(content)
        return jsonData, ct
    except Exception as e:
        pass
        #print(e)
    return jsonData, ct