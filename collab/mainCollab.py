import csv
import pandas as pd
import ssl
from collab import collaboration

ssl._create_default_https_context = ssl._create_unverified_context

def main():
    path = '../data/'
    data_file = open(path + 'Repos_collab50_Contri.csv', mode='w', newline='', encoding='utf-8')
    data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer.writerow(
        ['No', 'mainline_repo', 'contributors_ml', 'variant_repo', 'contributors_vr', 'contributorsML', 'contributorsVR'])

    df_data = pd.read_csv(path + 'Repos_50Commits.csv',sep=';')
    No = df_data.No.values.tolist()
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()

    ct = 0
    counter = 0
    for i in range(len(mainline_repo)):
        print(i, mainline_repo[i])

        issuesMlv, ct = collaboration.repos_issues_counts(mainline_repo[i], ct)
        prMlv, ct = collaboration.repos_pr_counts(mainline_repo[i], ct)
        contribMlv, contrib_arraysMlv, ct = collaboration.repos_contrib_counts(mainline_repo[i], ct)

        issuesFv, ct = collaboration.repos_issues_counts(variant_repo[i], ct)
        prFv, ct = collaboration.repos_pr_counts(variant_repo[i], ct)
        contribFv, contrib_arraysFv, ct = collaboration.repos_contrib_counts(variant_repo[i], ct)

        data_writer.writerow(
            [No[i], mainline_repo[i], contribMlv, variant_repo[i], contribFv, '/'.join(map(str,contrib_arraysMlv)), '/'.join(map(str,contrib_arraysFv))]
             )
    data_file.close()

if __name__ == '__main__':
    main()