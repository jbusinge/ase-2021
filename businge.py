import csv
import pandas as pd
import tokens
import urllib.request
import json
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

class GitURL:
    def __init__(self, url, ct):
        self.ct = ct
        self.url = url
    def getResponse(self):

        try:
            if self.ct == len(tokens.allTokens):
                self.ct = 0
            reqr = urllib.request.Request(self.url)
            reqr.add_header('Authorization','token %s' % tokens.allTokens[self.ct])
            opener = urllib.request.build_opener(urllib.request.HTTPHandler(debuglevel=1))
            content = opener.open(reqr).read()
            self.ct += 1
            jsonData = json.loads(content)
            return jsonData, self.ct

        except Exception as e:
            print(self.url + '   getResponse')
            print(e)
            return None, self.ct
    def url_header(self):

        try:
            if self.ct == len(tokens.allTokens):
                self.ct = 0
            reqr = urllib.request.Request(self.url)
            reqr.add_header('Accept', 'application/vnd.github.mercy-preview+json')
            reqr.add_header('Authorization', 'token %s' % tokens.allTokens[self.ct])
            opener = urllib.request.build_opener(urllib.request.HTTPHandler(debuglevel=1))
            content = opener.open(reqr).read()
            self.ct += 1
            jsonData = json.loads(content)
            return jsonData, self.ct
        except Exception as e:
            print(self.url + '  url_header(self)')
            print(e)
            return None, self.ct

class GitHubMeta:
    def __init__(self, repo, ct):
        self.ct = ct
        self.repo = repo

    def commit_counts_(self, created_at=None):
        p = 1
        total_contrib = 0
        ret = False
        while True:
            if created_at != None:
                url2 = 'https://api.github.com/repos/' + self.repo + '/commits?page=' + str(p) + '&per_page=100&since='+created_at
            else:
                url2 = 'https://api.github.com/repos/' + self.repo + '/commits?page=' + str(p) + '&per_page=100'
            com_arrays, self.ct = GitURL(url2, self.ct).getResponse()
            p += 1
            if com_arrays == None:
                break
            if com_arrays != None:
                if len(com_arrays) == 0:
                    break
                total_contrib += len(com_arrays)
            if total_contrib >= 50:
                ret = True
                break
            else:
                ret = False
                break

                # if total_contrib
                # if total_contrib % 500 == 0:
                #     print(' ---- commits: ', total_contrib)
        return ret, self.ct

def main():
    path = 'data/'
    data_file = open(path + 'Repos_50commits.csv', mode='w', newline='', encoding='utf-8')
    data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer.writerow(
        ['No', 'mainline', 'mainline_repo', 'mainline_repoid', 'variant', 'variant_repo', "variant_repoid",
         'target_mainline', 'target_variant', 'packages_mainline', 'projects_mainline', 'packages_variant',
         'projects_variant', 'version_mainline', 'version_variant'])

    df_data = pd.read_csv(path + 'families.csv', sep=";")
    No = df_data.No.values.tolist()
    mainline = df_data.mainline.values.tolist()
    mainline_repo = df_data.mainline_repo.values.tolist()
    mainline_repoid = df_data.mainline_repoid.values.tolist()
    variant = df_data.variant.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()
    variant_repoid = df_data.variant_repoid.values.tolist()
    target_mainline = df_data.target_mainline.values.tolist()
    target_variant = df_data.target_variant.values.tolist()
    packages_mainline = df_data.packages_mainline.values.tolist()
    projects_mainline = df_data.projects_mainline.values.tolist()
    packages_variant = df_data.packages_variant.values.tolist()
    projects_variant = df_data.projects_variant.values.tolist()
    version_mainline = df_data.version_mainline.values.tolist()
    version_variant = df_data.version_variant.values.tolist()
    ct = 0
    counter = 0
    for i in range(len(mainline_repo)):
        print(i, mainline_repo[i])
        ## Mainline commits

        #TODO:: Get the creation date for the fork
        url2 = 'https://api.github.com/repos/' + variant_repo[i]
        fork_meta, ct = GitURL(url2, ct).getResponse()
        if fork_meta != None:

            created_at = fork_meta['created_at']
            # Get forks commits
            fp_commits, ct = GitHubMeta(variant_repo[i], ct).commit_counts_(created_at)
            if fp_commits:
                # ml_commits, ct = GitHubMeta(mainline_repo[i], ct).commit_counts_()
                data_writer.writerow(
                    [No[i], mainline[i], mainline_repo[i], mainline_repoid[i], variant[i], variant_repo[i],
                     variant_repoid[i],
                     target_mainline[i], target_variant[i], packages_mainline[i], projects_mainline[i], packages_variant[i],
                     projects_variant[i], version_mainline[i], version_variant[i]])
    data_file.close()
if __name__ == '__main__':
    main()