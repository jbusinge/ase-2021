import requests

def jsonAPIReturn(url, token):
    return requests.get(url, headers={'Authorization': token}).json()