Dear Active GitHub Contributor,

We are carrying out an empirical investigation on software families on GitHub (i.e., mainlines and their variants). A variant is a software repository that is forked from another repository (mainline) and thereafter they evolve into independent projects being maintained in parallel. The reason we are contacting you is because we have identified your GitHub repository "Aurorastation/Aurora.3" as a variant of the mainline "Baystation12/Baystation12".

I am conducting this research as a postdoctoral researcher at the University of Antwerp in the context of a Belgian interuniversity research collaboration. With this survey we aim to identify and understand the reasons that led you to create or to participate in a fork of an existing project. Based on the results of this survey, we plan to develop open-source recommendation tools for automating the extraction and integration of patches between variants in a software family.Details of the research project and the data policy can be found in the Informed consent form that is part of the survey link below.

If you are actively involved in creating the aforementioned variant, kindly answer the questions in the survey link below. Your participation in this project consists of filling out an online questionnaire, lasting about 15 minutes.

To help us link your survey answers to the aforementioned variant repository, in the survey we shall kindly ask you to restate the name of the variant in the survey.

https://github.com/johnxu21/variants-survey/blob/main/Survey.pdf


Thank you so much for participating in our survey.

Best regards,

John Businge
Post Doctoral Research
University of Antwerp