import csv
from src.quantum.codeanalyser.v2.TOKENS import get_tokens
import urllib.request
import json

class GitHub:
    def __init__(self, url, ct):
        self.ct = ct
        self.url = url
    def getResponse(self):
        jsonData = None
        try:
            if self.ct == len(get_tokens()):
                self.ct = 0
            reqr = urllib.request.Request(self.url)
            reqr.add_header('Authorization', 'token %s' % get_tokens()[self.ct])
            opener = urllib.request.build_opener(urllib.request.HTTPHandler(debuglevel=1))
            content = opener.open(reqr).read()
            self.ct += 1
            jsonData = json.loads(content)
            #return jsonData, self.ct
        except Exception as e:
            pass
            #print(e)
        return jsonData, self.ct
    def url_header(self):
        jsonData = None
        try:
            if self.ct == len(get_tokens()):
                self.ct = 0
            reqr = urllib.request.Request(self.url)
            reqr.add_header('Accept', 'application/vnd.github.mercy-preview+json')
            reqr.add_header('Authorization', 'token %s' % get_tokens()[self.ct])
            opener = urllib.request.build_opener(urllib.request.HTTPHandler(debuglevel=1))
            content = opener.open(reqr).read()
            self.ct += 1
            jsonData = json.loads(content)
            return jsonData, self.ct
        except Exception as e:
            pass
            #print(e)
        return jsonData, self.ct

class GitHubMeta:
    def __init__(self, repo, ct):
        self.ct = ct
        self.repo = repo
    def repos_contrib_counts_(self):
        p = 1
        total_contrib = 0
        while True:
            url2 = 'https://api.github.com/repos/'+self.repo+'/contributors?page='+str(p)+'&per_page=100'
            contrib_arrays, self.ct = GitHub(url2, self.ct).getResponse()
            p += 1
            if contrib_arrays != None:
                if len(contrib_arrays) == 0:
                    break
                total_contrib += len(contrib_arrays)
            else:
                break
        return total_contrib, self.ct
    def repos_pr_counts_(self):
        p = 1
        total_contrib = 0
        while True:
            url2 = 'https://api.github.com/repos/'+self.repo+'/pulls?state=all&page='+str(p)+'&per_page=100&'
            contrib_arrays, self.ct = GitHub(url2, self.ct).getResponse()
            p += 1
            if contrib_arrays != None:
                if len(contrib_arrays) == 0:
                    break
                total_contrib += len(contrib_arrays)
                if total_contrib % 500 == 0:
                    print(' ---- pr: ', total_contrib)
            else:
                break
        return total_contrib, self.ct
    def repos_issues_counts_(self):
        p = 1
        total_contrib = 0
        while True:
            url2 = 'https://api.github.com/repos/'+self.repo+'/issues?state=all&page='+str(p)+'&per_page=100'
            contrib_arrays, self.ct = GitHub(url2, self.ct).getResponse()
            p += 1
            if contrib_arrays != None:
                if len(contrib_arrays) == 0:
                    break
                total_contrib += len(contrib_arrays)
                if total_contrib%500 == 0:
                    print(' ---- issues: ', total_contrib)
            else:
                break
        return total_contrib, self.ct
    def repos_commits_counts_(self):
        p = 1
        total_contrib = 0
        while True:
            url2 = 'https://api.github.com/repos/'+self.repo+'/commits?page='+str(p)+'&per_page=100'
            contrib_arrays, self.ct = GitHub(url2, self.ct).getResponse()
            p += 1
            if contrib_arrays != None:
                if len(contrib_arrays) == 0:
                    break
                total_contrib += len(contrib_arrays)
                if total_contrib%500 == 0:
                    print(' ---- commits: ', total_contrib)
            else:
                break
        return total_contrib, self.ct
    def repos_releases_counts_(self):
        p = 1
        total_contrib = 0
        while True:
            url2 = 'https://api.github.com/repos/'+self.repo+'/releases?page='+str(p)+'&per_page=100'
            contrib_arrays, self.ct = GitHub(url2, self.ct).getResponse()
            p += 1
            if contrib_arrays != None:
                if len(contrib_arrays) == 0:
                    break
                total_contrib += len(contrib_arrays)
                if total_contrib%500 == 0:
                    print(' ---- releases: ', total_contrib)
            else:
                break
        return total_contrib, self.ct

def main():
    path_output = '/Users/mosesopenja/Documents/summer2021/thesis/data2/'
    data_file = open(path_output + 'Repos_metrics_v3.csv', mode='w', newline='',
                                  encoding='utf-8')
    data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer.writerow(['Repos', 'topic', 'commits', 'contributors', 'issues','pulls', 'releases','size','stars','forks','open_issues','archived','created_at', 'updated_at','language','homepage', 'description'])
    proj_list = []
    suggested_topics = ['autonomy','autonomous','artifical-inteligent','self-driving-car','openpilot','autopilot']
    for word in suggested_topics:
        url = 'https://api.github.com/search/topics?q='+word
        ct = 0
        github = GitHub(url, ct)
        json_data, ct = github.url_header()  # gitHub.readUrl(get_tokens()[0])
        if json_data != None:
            print(word,json_data['total_count'])
            for obj_ in json_data['items']:
                topic = obj_['name']
                print(topic)
                url2 = 'https://api.github.com/search/repositories?q='+topic+'+forks:>=50+fork:false&sort=forks&order=desc&page=1&per_page=100&created_at<=2020-01-10T55:55:00Z'
                data, ct = GitHub(url2, ct).getResponse()
                if data != None:
                    print(topic, ' total repos:', data['total_count'])
                    for obj in data['items']:
                        if not obj['full_name'] in proj_list:
                            proj_list.append(obj['full_name'])
                            issues, ct = GitHubMeta(obj['full_name'], ct).repos_issues_counts_()
                            pr, ct = GitHubMeta(obj['full_name'], ct).repos_pr_counts_()
                            contrib, ct = GitHubMeta(obj['full_name'], ct).repos_contrib_counts_()
                            release, ct = GitHubMeta(obj['full_name'], ct).repos_releases_counts_()
                            commits, ct = GitHubMeta(obj['full_name'], ct).repos_commits_counts_()

                            data_writer.writerow(
                                [obj['full_name'], topic, commits, contrib, issues, pr, release, obj['size'],
                                 obj['stargazers_count'], obj['forks'], obj['open_issues'], obj['archived'], obj['created_at'], obj['updated_at'], obj['language'],
                                 obj['homepage'], obj['description']])

                            #data_writer.writerow([obj['full_name'],topic, commits, contrib, issues, pr, release, ])

    data_file.close()



if __name__ == '__main__':
    main()






