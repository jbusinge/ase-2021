from collab import header_jsonData


def repos_pr(repo, stop_date, ct):
    p = 1
    total_prs = 0

    pr_owner = []
    created_at = []
    merged_at = []
    closed_at = []
    pr_no = []
    repo_origin = []
    repo_destination = []
    issue_url = []

    while True:
        url2 = 'https://api.github.com/repos/' + repo + '/pulls?state=closed&page=' + str(p) + '&per_page=100&'
        pr_arrays, ct = header_jsonData.getResponse(url2, ct)
        p += 1
        if pr_arrays is not None:
            for obj in pr_arrays:
                if obj['closed_at'] is not None:
                    if obj['closed_at'] <= stop_date:
                        closed_at.append(obj['closed_at'])
                        # print(obj['number'])
                        if obj['merged_at'] is not None:
                            merged_at.append(obj['merged_at'])
                        else:
                            merged_at.append('')
                        created_at.append(obj['created_at'])
                        pr_no.append(obj['number'])
                        pr_owner.append(obj['user']['login'])
                        if obj['base']['repo'] is not None and obj['base']['repo']['full_name'] is not None:
                            repo_destination.append(obj['base']['repo']['full_name'])
                        else:
                            repo_destination.append('')
                        if obj['head']['repo'] is not None and obj['head']['repo']['full_name'] is not None:
                            repo_origin.append(obj['head']['repo']['full_name'])
                        else:
                            repo_origin.append('')
                        issue_url.append(obj['issue_url'])

            if len(pr_arrays) == 0:
                break
            total_prs += len(pr_arrays)
            if total_prs % 500 == 0:
                print(' ---- pr: ', total_prs)
        else:
            break
    return pr_no, created_at,  closed_at, merged_at, repo_origin, repo_destination, pr_owner, issue_url, ct