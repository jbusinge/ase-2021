import json
import ssl

import requests
import urllib3

import tokens_ci
import tokens_circle_ci

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
ssl._create_default_https_context = ssl._create_unverified_context

def builds_circle_ci(repo, ct):
    builds = 0
    if ct == len(tokens_circle_ci.allTokens):
        ct = 0

    url_com = 'https://circleci.com/api/v1.1/project/gh/' + repo + '?limit=100&circle-token=' + \
              tokens_circle_ci.allTokens[ct]
    ct += 1
    req_com = requests.get(url_com)
    jsonData_com = json.loads(req_com.text)
    # print('len(jsonData_com) = ', len(jsonData_com))
    # print(ct, tokens_circle_ci.allTokens[ct])
    if len(jsonData_com) != 0:
        builds = jsonData_com[0]['build_num']
        print('builds= ', jsonData_com[0]['build_num'])
    else:
        print('builds= ', str(builds))

    return builds, ct

def builds_travis_ci(repo, ct):
    jsonData = None
    pr_builds = []
    no_build = 0
    ct = 0
    builds = 0

    travis_ci_arrays = []
    if ct == len(tokens_ci.allTokens):
        ct = 0
    headers = {'Authorization': 'Token ' + tokens_ci.allTokens[ct]}
    url_com = 'https://api.travis-ci.com/repos/' + repo + '/builds'
    url_org = 'https://api.travis-ci.org/repos/' + repo + '/builds'
    ct += 1

    req_com = requests.get(url_com, headers=headers, verify=False)
    req_org = requests.get(url_org)
    jsonData_com = json.loads(req_com.text)
    jsonData_org = json.loads(req_org.text)

    if len(jsonData_com) != 0:
        # print(len(jsonData_com))
        print(jsonData_com[0]['number'])
        builds = jsonData_com[0]['number']
        print('jsonData_com success')
    elif len(jsonData_org) != 0:
        # print(json.dumps(jsonData_org, indent=4))
        # print(len(jsonData_org))
        print(jsonData_org[0]['number'])
        builds = jsonData_org[0]['number']
        print('jsonData_org success')
    else:
        print('No Builds')

    return builds, ct


def builds_travis_ci_pr(repo, ct):
    pr_builds = []
    no_build = 0
    ct = 0
    builds_pr = 0

    travis_ci_arrays = []
    if ct == len(tokens_ci.allTokens):
        ct = 0
    headers = {'Authorization': 'Token ' + tokens_ci.allTokens[ct]}
    url_com = 'https://api.travis-ci.com/repos/' + repo + '/builds'
    url_org = 'https://api.travis-ci.org/repos/' + repo + '/builds'
    ct += 1

    req_com = requests.get(url_com, headers=headers, verify=False)
    req_org = requests.get(url_org)
    jsonData_com = json.loads(req_com.text)
    jsonData_org = json.loads(req_org.text)
    while True:
        if len(jsonData_com) != 0:
            after_number = jsonData_com[len(jsonData_com) - 1]['number']
            for i in range(len(jsonData_com)):
                builds = jsonData_com[0]['number']
                # print('jsonData_com success')
                if jsonData_com[i]['event_type'] == 'pull_request':
                    builds_pr += 1
            jsonData_com, after_number, ct = jsondata_more(repo, after_number, 'com', ct)
            if len(jsonData_com) == 0:
                return builds_pr, ct

        elif len(jsonData_org) != 0:
            after_number = jsonData_org[len(jsonData_org) - 1]['number']
            for i in range(len(jsonData_org)):
                # print(jsonData_org[0]['number'])
                if jsonData_org[i]['event_type'] == 'pull_request':
                    builds_pr += 1
                # print('jsonData_org success')
            jsonData_org, after_number, ct = jsondata_more(repo, after_number, 'org', ct)
            if len(jsonData_org) == 0:
                return builds_pr, ct
        else:
            print('No Builds')
            break

        if builds_pr % 200 == 0:
            print(' ---- builds_pr: ', builds_pr)

    return builds_pr, ct


def jsondata_more(repo, after_number, com_org, ct):
    jsondata = None

    if 'com' in com_org:
        if ct == len(tokens_ci.allTokens):
            ct = 0
        headers = {'Authorization': 'Token ' + tokens_ci.allTokens[ct]}
        url_com = 'https://api.travis-ci.com/repos/' + repo + '/builds?after_number=' + after_number
        req_com = requests.get(url_com, headers=headers, verify=False)
        jsondata = json.loads(req_com.text)
        ct += 1
    else:
        url_org = 'https://api.travis-ci.org/repos/' + repo + '/builds?after_number=' + after_number
        req_org = requests.get(url_org)
        jsondata = json.loads(req_org.text)

    return jsondata, after_number, ct
