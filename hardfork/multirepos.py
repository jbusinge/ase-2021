import csv

import pandas as pd


def main():
    path_data = '../data/hardfork/'

    df_data = pd.read_csv(path_data + 'mainline_variant_developer_details_hardfork_12.csv', sep=';')
    mainline = df_data.mainline.values.tolist()
    variant = df_data.variant.values.tolist()
    variant_developer = df_data.variant_maintainers_details.values.tolist()
    mainline_developer = df_data.mainline_maintainer_details.values.tolist()

    dict_keys = ['No', 'mainline', 'variant', 'mainline_maintainer_details', 'variant_maintainers_details']

    lst = []
    a = 1
    for i in range(len(mainline)):
        # print(i+1, mainline_developer[i])

        if not pd.isna(variant_developer[i]) and not pd.isna(mainline_developer[i]):
            variant_developer_arry = variant_developer[i].split('/')
            mainline_developer_arry = mainline_developer[i].split('/')
            flag = 0
            for obj_var in variant_developer_arry:
                varj = obj_var.split('#')
                for obj_main in mainline_developer_arry:
                    mainj = obj_main.split('#')
                    if varj[0] == mainj[0]:
                        print(i+1, mainline[i], variant[i])
                        dict_values = [a, mainline[i], variant[i], mainline_developer[i], variant_developer[i]]
                        dict_all = dict(zip(dict_keys, dict_values))
                        lst.append(dict_all)
                        a += 1
                        flag = 1
                        break
                if flag == 1:
                    break

            data_file = open(path_data + 'multirepos_hardfork_1.csv', mode='w', newline='',
                             encoding='utf-8')
            data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer.writerow(dict_keys)

            for obj in lst:
                data_writer_ml = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                data_writer_ml.writerow([obj['No'],
                                         obj['mainline'],
                                         obj['variant'],
                                         obj['mainline_maintainer_details'],
                                         obj['variant_maintainers_details']
                                         ])
            data_file.close()


if __name__ == '__main__':
    main()
