import csv
import pandas as pd

from collab.collaboration import dates
from collab.issues_collab import issue_comments
import ssl

from collab.issues_collab.merged_pulls import repos_pr

ssl._create_default_https_context = ssl._create_unverified_context


def is_not_blank(s):
    return bool(s and s.strip())


def main():
    path_ml = '../../data/data_new_1/ml/'
    path_vr = '../../data/data_new_1/vr/'
    path = '../../data/data_new_1/'

    df_data = pd.read_csv(path + 'pr_details_diff_22-33.csv', sep=';')
    No = df_data.No.values.tolist()
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()

    stop_date = '2021-01-09T00:00:00Z'

    ct = 0
    counter = 0
    # range(len(mainline_repo))
    for i in range(len(mainline_repo)):
        # counter += 1
        # if counter <= 24:
        #     print(i, mainline_repo[i])
        #     continue
        # else:

        file_ml = mainline_repo[i].replace('/', '_')
        data_file_ml = open(path_ml + file_ml + '.csv', mode='w', newline='', encoding='utf-8')
        data_writer_ml = csv.writer(data_file_ml, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer_ml.writerow(
            ['No', 'mainline_repo', 'pr_no', 'created_at', 'closed_at', 'merged_at', 'repo_origin_ml', 'repo_destin',
             'pr_owner', 'no_commenters', 'pr_commenters', 'pr_comments'])

        file_vr = variant_repo[i].replace('/', '_')
        data_file_vr = open(path_vr + file_vr + '.csv', mode='w', newline='', encoding='utf-8')
        data_writer_vr = csv.writer(data_file_vr, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer_vr.writerow(
            ['No', 'variant_repo', 'pr_no', 'created_at', 'closed_at', 'merged_at', 'repo_origin_ml', 'repo_destin',
             'pr_owner', 'no_commenters', 'pr_commenters', 'pr_comments'])

        # print(i, mainline_repo[i])

        created_at, updated_at, archived, ct = dates(variant_repo[i], ct)
        created_at1, updated_at1, archived1, ct = dates(mainline_repo[i], ct)

        dict_keys = ['No', 'mainline_repo', 'pr_no', 'created_at', 'closed_at', 'merged_at', 'repo_origin_ml',
                     'repo_destin',
                     'pr_owner', 'no_commenters', 'pr_commenters', 'pr_comments']

        if is_not_blank(created_at) and is_not_blank(created_at1):
            print(i, mainline_repo[i])
            print('print  = ' + created_at)
            pr_no_ml, created_at_ml, closed_at_ml, merged_at_ml, repo_origin_ml, repo_destin_ml, pr_owner_ml, issue_url_ml, ct = repos_pr(
                mainline_repo[i], stop_date, ct)
            # if not merged_at_ml:
            #     unmerged = 'yes'
            # else:
            #     unmerged = 'no'
            pr_no_vr, created_at_vr, closed_at_vr, merged_at_vr, repo_origin_vr, repo_destin_vr, pr_owner_vr, issue_url_vr, ct = repos_pr(
                variant_repo[i], stop_date, ct)



            for j in range(len(created_at_ml)):
                ml_comments, ml_pr_commenters, ct = issue_comments.comments(issue_url_ml[j], merged_at_ml[j], ct)
                if len(ml_comments) > 0:
                    # dict_values_ml = [j, mainline_repo[i], pr_no_ml[j], created_at_ml[j], closed_at_ml[j],
                    #                   repo_origin_ml[j],
                    #                   repo_destin_ml[j], pr_owner_ml[j], len(ml_pr_commenters),
                    #                   '/'.join(map(str, ml_pr_commenters)),
                    #                   ' '.join(map(str, ml_comments))]

                    data_writer_ml.writerow(
                        [j, mainline_repo[i], pr_no_ml[j], created_at_ml[j], closed_at_ml[j], merged_at_ml[j], repo_origin_ml[j],
                         repo_destin_ml[j], pr_owner_ml[j], len(ml_pr_commenters), '/'.join(map(str, ml_pr_commenters)),
                         ' '.join(map(str, ml_comments))]
                    )
            data_file_ml.close()
            print(path_ml + file_ml + '.csv written')

            print(i, variant_repo[i])
            print('print  = ' + created_at)

            for j in range(len(created_at_vr)):
                vr_comments, vr_pr_commenters, ct = issue_comments.comments(issue_url_vr[j], closed_at_vr[j], ct)
                if len(vr_comments) > 0:
                    # dict_values_vr = [j, variant_repo[i], pr_no_vr[j], created_at_vr[j], closed_at_vr[j], merged_at_vr[j],
                    #                   repo_origin_vr[j],
                    #                   repo_destin_vr[j], pr_owner_vr[j], len(vr_pr_commenters),
                    #                   '/'.join(map(str, vr_pr_commenters)),
                    #                   ' '.join(map(str, vr_comments))]

                    data_writer_vr.writerow(
                        [j, variant_repo[i], pr_no_vr[j], created_at_vr[j], closed_at_vr[j], merged_at_vr[j], repo_origin_vr[j],
                         repo_destin_vr[j], pr_owner_vr[j], len(vr_pr_commenters), '/'.join(map(str, vr_pr_commenters)),
                         ' '.join(map(str, vr_comments))]
                    )

            data_file_vr.close()
            print(path_vr + file_vr + '.csv written')


if __name__ == '__main__':
    main()
