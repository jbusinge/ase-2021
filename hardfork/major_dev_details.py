import csv

import pandas as pd

from collab.collaboration import dates, repos_issues, repos_prs
from hardfork.collaboration import dev_email_mainline, dev_email_variant


def main():
    path_data = '../data/hardfork/'

    df_data = pd.read_csv(path_data + 'variant_developer_emails_hardfork.csv', sep=';')
    mainline = df_data.mainline.values.tolist()
    variant = df_data.variant.values.tolist()

    dict_keys = ['No', 'mainline', 'variant', 'mainline_maintainer-details', 'variant_maintainers-details']

    ct = 0

    lst = []
    a = 1
    for i in range(82, len(mainline)):
        print(i+1, mainline[i], variant[i])
        created_at, updated_at, archived_vr, ct = dates(variant[i], ct)
        mainline_developer_details, ct = dev_email_mainline(mainline[i], created_at, ct)
        variant_developer_details, ct = dev_email_variant(variant[i], ct)
        print('mainline_developer_details', mainline_developer_details)
        print('variant_developer_details', variant_developer_details)

        dict_values = [a, mainline[i], variant[i], mainline_developer_details, variant_developer_details]
        a += 1

        dict_all = dict(zip(dict_keys, dict_values))
        lst.append(dict_all)

        data_file = open(path_data + 'mainline_variant_developer_details_hardfork_3.csv', mode='w', newline='',
                         encoding='utf-8')
        data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(dict_keys)

        for obj in lst:
            data_writer_ml = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer_ml.writerow([obj['No'],
                                     obj['mainline'],
                                     obj['variant'],
                                     obj['mainline_maintainer-details'],
                                     obj['variant_maintainers-details']
                                     ])
        data_file.close()


if __name__ == '__main__':
    main()