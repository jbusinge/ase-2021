import csv
import pandas as pd

from collab.collaboration import dates, repos_issues, repos_prs


def main():
    path_data = '../data/'

    df_data = pd.read_csv(path_data + 'fork_of_3.csv', sep=';')
    mainline = df_data.mainline.values.tolist()
    variant = df_data.variant.values.tolist()
    # created_at = df_data.created_at_vr.values.tolist()

    until = '2021-02-01T00:00:00Z'

    dict_keys = ['No', 'mainline', 'total_issues_ml', 'total_pr_ml', 'issue_users_ml', 'issue_commenters_ml', 'pr_users_ml',
                 'pr_commenters_ml', 'variant', 'total_issues_vr', 'total_pr_vr', 'issue_users_vr',
                 'issue_commenters_vr', 'pr_users_vr', 'pr_commenters_vr',
                 'common_issue_users', 'common_issue_commenters', 'common_pr_users', 'common_pr_commenters',
                 'text_common_issue_users', 'text_common_issue_commenters', 'text_common_pr_users',
                 'text_common_pr_commenters',
                 'text_issue_users_ml', 'text_issue_commenters_ml', 'text_pr_users_ml', 'text_pr_commenters_ml',
                 'text_issue_users_vr', 'text_issue_commenters_vr', 'text_pr_users_vr', 'text_pr_commenters_vr'
                 ]

    # data_file = open('fork_of_details.csv', mode='w', newline='',
    #                  encoding='utf-8')
    # data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    # data_writer.writerow(dict_keys)

    ct = 0

    lst1 = []
    for i in range(len(mainline)):

        common_issue_users = []
        common_issue_commenters = []

        common_pr_users = []
        common_pr_commenters = []

        created_at, updated_at, archived, ct = dates(variant[i], ct)
        print(i, mainline[i])

        lst_pull_users_ml = []
        lst_issue_users_ml = []
        lst_issues_commenters_ml = []
        lst_pull_commenters_ml = []

        total_issues_ml, issue_users_ml, issue_commenters_ml, dct_issues_users_ml, dct_issues_commenters_ml, ct = repos_issues(
            mainline[i], created_at, until, ct)
        total_pulls_ml, pull_users_ml, pull_commenters_ml, dct_pull_users_ml, dct_pull_commenters_ml, ct = repos_prs(
            mainline[i], created_at, until, ct)

        print('total_issues_ml = ', str(total_issues_ml))
        print('total_pulls_ml = ', str(total_pulls_ml))


        print(i, variant[i])

        total_issues_vr, issue_users_vr, issue_commenters_vr, dct_issues_users_vr, dct_issues_commenters_vr, ct = repos_issues(
            variant[i], created_at, until, ct)
        total_pulls_vr, pull_users_vr, pull_commenters_vr, dct_pull_users_vr, dct_pull_commenters_vr, ct = repos_prs(
            variant[i], created_at, until, ct)

        print('total_issues_vr = ', str(total_issues_vr))
        print('total_pulls_vr = ', str(total_pulls_vr))

        # print(dict_issues_users_ml)
        for k, v in dct_issues_users_ml[::-1]:
            lst_issue_users_ml.append(k + '-' + str(v))
        str_issues_users_ml = '/'.join(map(str, lst_issue_users_ml))

        for k, v in dct_pull_users_ml[::-1]:
            lst_pull_users_ml.append(k + '-' + str(v))
        str_pull_users_ml = '/'.join(map(str, lst_pull_users_ml))

        for k, v in dct_issues_commenters_ml[::-1]:
            lst_issues_commenters_ml.append(k + '-' + str(v))
        str_issues_commenters_ml = '/'.join(map(str, lst_issues_commenters_ml))

        for k, v in dct_pull_commenters_ml[::-1]:
            lst_pull_commenters_ml.append(k + '-' + str(v))
        str_pull_commenters_ml = '/'.join(map(str, lst_pull_commenters_ml))

        lst_pull_users_vr = []
        lst_issue_users_vr = []
        lst_issues_commenters_vr = []
        lst_pull_commenters_vr = []

        for k, v in dct_pull_users_vr[::-1]:
            lst_pull_users_vr.append(k + '-' + str(v))
        str_pull_users_vr = '/'.join(map(str, lst_pull_users_vr))

        for k, v in dct_issues_users_vr[::-1]:
            lst_issue_users_vr.append(k + '-' + str(v))
        str_issues_users_vr = '/'.join(map(str, lst_issue_users_vr))

        for k, v in dct_issues_commenters_vr[::-1]:
            lst_issues_commenters_vr.append(k + '-' + str(v))
        str_issues_commenters_vr = '/'.join(map(str, lst_issues_commenters_vr))

        for k, v in dct_pull_commenters_vr[::-1]:
            lst_pull_commenters_vr.append(k + '-' + str(v))
        str_pull_commenters_vr = '/'.join(map(str, lst_pull_commenters_vr))


        print('lst_issue_users_vr = ', str(len(lst_issue_users_vr)), ' = ', lst_issue_users_vr)
        print('lst_pull_users_ml = ', str(len(lst_pull_users_ml)), ' = ', lst_pull_users_ml)
        print('lst_pull_users_vr = ', str(len(lst_pull_users_vr)), ' = ', lst_pull_users_vr)

        if len(issue_users_ml) > 0 and len(issue_users_vr) > 0:
            for j in range(len(issue_users_ml)):
                if issue_users_ml[j] in issue_users_vr:
                    common_issue_users.append(issue_users_ml[j])

        if len(issue_commenters_ml) > 0 and len(issue_commenters_vr) > 0:
            for j in range(len(issue_commenters_ml)):
                if issue_commenters_ml[j] in issue_commenters_vr:
                    common_issue_commenters.append(issue_commenters_ml[j])

        if len(pull_users_ml) > 0 and len(pull_users_vr) > 0:
            for j in range(len(pull_users_ml)):
                if pull_users_ml[j] in pull_users_vr:
                    common_pr_users.append(pull_users_ml[j])

        if len(pull_commenters_ml) > 0 and len(pull_commenters_vr) > 0:
            for j in range(len(pull_commenters_ml)):
                if pull_commenters_ml[j] in pull_commenters_vr:
                    common_pr_commenters.append(pull_commenters_ml[j])

        dict_values = [i+1, mainline[i], total_issues_ml, total_pulls_ml, len(issue_users_ml), len(issue_commenters_ml),
                       len(pull_users_ml), len(pull_commenters_ml),
                       variant[i], total_issues_vr, total_pulls_vr,
                       len(issue_users_vr), len(issue_commenters_vr), len(pull_users_vr), len(pull_commenters_vr),
                       len(common_issue_users), len(common_issue_commenters), len(common_pr_users),
                       len(common_pr_commenters),
                       '/'.join(map(str, common_issue_users)), '/'.join(map(str, common_issue_commenters)),
                       '/'.join(map(str, common_pr_users)), '/'.join(map(str, common_pr_commenters)),
                       str_issues_users_ml, str_issues_commenters_ml,
                       str_pull_users_ml, str_pull_commenters_ml,
                       str_issues_users_vr, str_issues_commenters_vr,
                       str_pull_users_vr, str_pull_commenters_vr
                       ]
        dict_all = dict(zip(dict_keys, dict_values))
        lst1.append(dict_all)

        print('common_issue_users = ', str(len(common_issue_users)), ' = ', common_issue_users)
        print('common_issue_commenters = ', str(len(common_issue_commenters)), ' = ', common_issue_commenters)
        print('common_pr_users = ', str(len(common_pr_users)), ' = ', common_pr_users)
        print('common_pr_commenters = ', str(len(common_pr_commenters)), ' = ', common_pr_commenters)

        print('***********************************')

        data_file = open('fork_details_18.csv', mode='w', newline='',
                         encoding='utf-8')
        data_writer = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(dict_keys)

        for obj in lst1:
            data_writer_ml = csv.writer(data_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer_ml.writerow([ obj['No'],
                obj['mainline'], obj['total_issues_ml'], obj['total_pr_ml'], obj['issue_users_ml'], obj['issue_commenters_ml'],
                obj['pr_users_ml'], obj['pr_commenters_ml'],
                obj['variant'], obj['total_issues_vr'], obj['total_pr_vr'], obj['issue_users_vr'],
                obj['issue_commenters_vr'], obj['pr_users_vr'], obj['pr_commenters_vr'],
                obj['common_issue_users'], obj['common_issue_commenters'], obj['common_pr_users'],
                obj['common_pr_commenters'],
                obj['text_common_issue_users'], obj['text_common_issue_commenters'], obj['text_common_pr_users'],
                obj['text_common_pr_commenters'],
                obj['text_issue_users_ml'], obj['text_issue_commenters_ml'], obj['text_pr_users_ml'],
                obj['text_pr_commenters_ml'],
                obj['text_issue_users_vr'], obj['text_issue_commenters_vr'], obj['text_pr_users_vr'],
                obj['text_pr_commenters_vr']])
        data_file.close()


if __name__ == '__main__':
    main()
