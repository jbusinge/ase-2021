import csv
import ssl

import pandas as pd

from collab import collaboration

ssl._create_default_https_context = ssl._create_unverified_context


def main():
    path = '../../data-ci/'
    data_file = open(path + 'pr_details_ci.csv', mode='w', newline='', encoding='utf-8')
    data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    data_writer.writerow(
        ['No', 'mainline_repo', 'closed_pr_ml', 'merged_pr_ml', 'variant_repo', 'closed_pr_vr', 'merged_pr_vr', 'ml_build', 'vr_build'])

    df_data = pd.read_csv(path + 'pr_details_final_2.csv', sep=';')
    No = df_data.No.values.tolist()
    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()
    closed_pr_ml = df_data.closed_pr_ml.values.tolist()
    merged_pr_ml = df_data.merged_pr_ml.values.tolist()
    closed_pr_vr = df_data.closed_pr_vr.values.tolist()
    merged_pr_vr = df_data.merged_pr_vr.values.tolist()

    ct = 0
    counter = 0
    for i in range(len(mainline_repo)):
        print(i, mainline_repo[i])

        ml_build, ct = collaboration.ci_integration(mainline_repo[i], ct)
        vr_build, ct = collaboration.ci_integration(variant_repo[i], ct)
        print('****** ' + mainline_repo[i], ml_build)
        print('****** ' + variant_repo[i], vr_build)
        print()

        data_writer.writerow(
            [No[i], mainline_repo[i], closed_pr_ml[i], merged_pr_ml[i], variant_repo[i], closed_pr_vr[i], merged_pr_vr[i], '/'.join(map(str, ml_build)), '/'.join(map(str, vr_build))]
        )
    data_file.close()


if __name__ == '__main__':
    main()
