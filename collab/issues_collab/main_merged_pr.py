import ssl

import csv
import pandas as pd

from collab.collaboration import repos_merged_pr_counts, dates

ssl._create_default_https_context = ssl._create_unverified_context


def is_not_blank(s):
    return bool(s and s.strip())


def main():
    path = '../../data/data_new/'

    df_data = pd.read_csv(path + 'diff_22-33.csv', sep=';')

    mainline_repo = df_data.mainline_repo.values.tolist()
    variant_repo = df_data.variant_repo.values.tolist()

    stop_date = '2021-01-09T00:00:00Z'

    data_file = open(path + 'pr_details_diff_44-55_2.csv', mode='w', newline='', encoding='utf-8')



    dict_keys = ['No', 'mainline_repo', 'closed_pr_ml', 'merged_pr_ml', 'variant_repo', 'closed_pr_vr', 'merged_pr_vr']
    ct = 0
    counter = 1
    ctr = 1
    # range(len(mainline_repo))
    lst = []

    for i in range(len(mainline_repo)):

        # if i < 391:
        #     continue
        # else:
        created_at, updated_at, archived, ct = dates(variant_repo[i], ct)
        created_at1, updated_at1, archived1, ct = dates(mainline_repo[i], ct)

        if is_not_blank(created_at) and is_not_blank(created_at1):
            # counter += 1
            # if counter <= 10:
            #     # print(i, mainline_repo[i])
            #     continue
            # else:
            print(i + 1, mainline_repo[i], variant_repo[i])
            file_ml = mainline_repo[i].replace('/', '_')

            closed_pr_ml, merged_pr_ml, ct = repos_merged_pr_counts(mainline_repo[i], stop_date, ct)
            closed_pr_vr, merged_pr_vr, ct = repos_merged_pr_counts(variant_repo[i], stop_date, ct)

            if closed_pr_ml > 2 and closed_pr_vr > 2:
                print(i+1, ctr, 'merged_pr_ml = ' + str(closed_pr_ml), str(merged_pr_ml),
                      'merged_pr_vr = ' + str(closed_pr_vr), str(merged_pr_vr))

                dict_values = [ctr, mainline_repo[i], closed_pr_ml, merged_pr_ml, variant_repo[i], closed_pr_vr,
                               merged_pr_vr]
                ctr += 1
                dict_all = dict()
                dict_all = dict(zip(dict_keys, dict_values))
                lst.append(dict_all)
                # print(dict_all)

                data_file = open(path + 'pr_details_diff_22-33.csv', mode='w', newline='', encoding='utf-8')
                data_writer_ml = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                data_writer_ml.writerow(
                    ['No', 'mainline_repo', 'closed_pr_ml', 'merged_pr_ml', 'variant_repo', 'closed_pr_vr', 'merged_pr_vr'])
                for obj in lst:
                    # print('obj[No]', obj['merged_pr_vr'])
                    # print(obj['No'], obj['mainline_repo'], obj['closed_pr_ml'], obj['merged_pr_ml'],
                    #       obj['variant_repo'], obj['closed_pr_vr'], obj['merged_pr_vr'])
                    data_writer_ml = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    data_writer_ml.writerow(
                        [obj['No'], obj['mainline_repo'], obj['closed_pr_ml'], obj['merged_pr_ml'], obj['variant_repo'],
                         obj['closed_pr_vr'], obj['merged_pr_vr']])
                    # [i, mainline_repo[i], closed_pr_ml, merged_pr_ml, variant_repo[i], closed_pr_vr, merged_pr_vr])
            counter += 1

        data_file.close()


if __name__ == '__main__':
    main()
